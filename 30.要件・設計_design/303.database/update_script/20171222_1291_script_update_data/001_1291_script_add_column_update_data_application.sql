ALTER TABLE `application`
	ADD COLUMN `status_desc` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `status_cd`;

#Update status_desc exists records
UPDATE application
SET
  status_desc = (CASE WHEN (application_type_id = 14 AND status_cd = 3) THEN 1
      WHEN (application_type_id = 21 AND status_cd = 2) THEN 1
      WHEN (application_type_id = 22 AND status_cd = 2) THEN 1
      WHEN (application_type_id = 23 AND status_cd = 3) THEN 1
      WHEN (application_type_id = 24 AND status_cd = 4) THEN 1
      WHEN (application_type_id = 25 AND status_cd = 4) THEN 1
      WHEN (application_type_id IN (1,2,3,4,5,6,7,8,9,10,11,12,13,15,16,17,18,19,20,26,27) AND (status_cd = 3 OR status_cd =5)) THEN 1
      ELSE 0 END);
UPDATE application SET status_desc = 2 WHERE status_cd = 9;