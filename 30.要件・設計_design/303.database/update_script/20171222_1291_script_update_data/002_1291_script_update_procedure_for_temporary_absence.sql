SET NAMES utf8;
DROP PROCEDURE IF EXISTS `sp_leave_absence_application_cancel`;
delimiter //
CREATE PROCEDURE `sp_leave_absence_application_cancel`(
	IN `pr_application_id` INT
,
	IN `pr_lastup_account_id` INT
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'Cancel leave absence application'
cancel_leaving_app:BEGIN
    DECLARE `_rollback` BOOL DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
	 /* get application type by column name */
	 SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');

    SET @status_cd = (SELECT status_cd FROM application WHERE id = pr_application_id and application_type_id = @app_type_id and disable = 0);
    SET @secretary_general_remand = fn_get_system_code('application_status', 'secretary_general_remand');
    SET @secretariat_input = fn_get_system_code('application_status', 'secretariat_input');

    /* check exists */
    IF @status_cd IS NULL THEN
        SELECT 1 as error, 'application not found' as errmsg;
        LEAVE cancel_leaving_app;
    END IF;

    SET @success = 1;
    START TRANSACTION;
    /* check status = 8 or 1*/
    IF (@status_cd <> @secretary_general_remand) AND (@status_cd <> @secretariat_input) THEN
        SELECT 2 as error, 'Cancel not yet ready' as errmsg;
        COMMIT;
        LEAVE cancel_leaving_app;
    ELSE
            /* disable application */
            SET @request_id = (select request_id from application where id = pr_application_id);
            SET @person_id = (select person_id from request where id = @request_id);
            SET @from_date = (select leave_absence_date_from from application_leave_absence_properties where application_id = pr_application_id);
            SET @to_date = (select leave_absence_date_to from application_leave_absence_properties where application_id = pr_application_id);

            UPDATE application
            SET
                status_cd = fn_get_system_code('application_status', 'secretariat_remand'),
                status_desc = 2,
                lastup_account_id = pr_lastup_account_id,
                lastup_datetime = now()
            WHERE
                id = pr_application_id and (status_cd = @secretary_general_remand OR status_cd = @secretariat_input) and disable = 0;

            /* update comment shift_attendance */
            CALL sp_update_comment_to_shift_attendance_by_person_id(@person_id, @from_date, @to_date, '', '休学予定', pr_lastup_account_id, @success);

    END IF;

    IF (`_rollback`) OR (@success = 0) THEN
        ROLLBACK;
        select 1 as error, 'exception error' as errmsg;
    ELSE
        COMMIT;
        select 0 as error;
    END IF;
END//
delimiter ;


SET NAMES utf8;
DROP PROCEDURE IF EXISTS `sp_leave_absence_application_group9_approval`;
delimiter //
CREATE PROCEDURE `sp_leave_absence_application_group9_approval`(
	IN `pr_application_id` INT
    ,
	IN `pr_confirmed_document` VARCHAR(255)
    ,
	IN `pr_reject_reason` VARCHAR(255)
    ,
	IN `pr_comment` VARCHAR(255)
    ,
	IN `pr_application_date` DATE
    ,
	IN `pr_leaving_date` DATE
    ,
	IN `pr_leaving_return_country_date` DATE
    ,
	IN `pr_leaving_approval_date_from` DATE,
	IN `pr_leaving_approval_date_to` DATE,
	IN `pr_leaving_return_country_phone_no` VARCHAR(20)
    ,
	IN `pr_leaving_return_country_mail_address` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address1` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address2` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address3` VARCHAR(255),
	IN `pr_leaving_reason` TEXT
    ,
	IN `pr_leaving_reason_detail` TEXT
    ,
	IN `pr_leaving_note` TEXT
    ,
	IN `pr_account_id` INT

)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'Head of School approval leave absence application '
update_app:BEGIN
	DECLARE `_rollback` BOOL DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
	/* get application type by column name */
	 SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');
	 SET @education_chief_approved = fn_get_system_code('application_status' , 'education_chief_approved');

    SET @status_cd = (SELECT status_cd FROM application WHERE id = pr_application_id and application_type_id = @app_type_id and disable = 0);
    /* check exists */
    IF @status_cd IS NULL THEN
        SELECT 1 as error, 'application not found' as errmsg;
        LEAVE update_app;
    END IF;

    START TRANSACTION;
    /* check status = 3 */
    IF @status_cd <> @education_chief_approved THEN
    		COMMIT;
        SELECT 2 as error, 'Chief not yet confirm' as errmsg;
        LEAVE update_app;
    ELSE
    		 /* get person_id */
		    SET @request_id = (SELECT request_id FROM application WHERE id = pr_application_id);
		    SET @person_id = (SELECT person_id FROM request WHERE id = @request_id);

    		/* update application status to 4 */

    		UPDATE application
    		SET
    			status_cd = fn_get_system_code('application_status' , 'school_chief_approved'),
    			status_desc = 1,
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
                application_date = pr_application_date
    		WHERE
    			id = pr_application_id;

    		/* Update application properties */
    		UPDATE application_leave_absence_properties
    		SET
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
				leave_absence_date_from = pr_leaving_date,
             leave_absence_date_to = pr_leaving_return_country_date,
             leave_absence_return_country_phone_no = pr_leaving_return_country_phone_no,
             leave_absence_return_country_mail_address = pr_leaving_return_country_mail_address,
             leave_absence_return_country_address1 = pr_leaving_return_country_address1,
             leave_absence_return_country_address2 = pr_leaving_return_country_address2,
             leave_absence_return_country_address3 = pr_leaving_return_country_address3,
             leave_absence_reason_id = pr_leaving_reason,
             leave_absence_reason_detail = pr_leaving_reason_detail,
             leave_absence_approval_date_from = pr_leaving_approval_date_from,
             leave_absence_approval_date_to = pr_leaving_approval_date_to,
             leave_absence_note = pr_leaving_note
			WHERE application_id = pr_application_id;

    		/* update application_approve data */
	 		UPDATE
	 			application_approve
	 		SET
	 			application_approver_id = pr_account_id,
	 			status_cd = 1,
	 			lastup_datetime = now(),
	 			lastup_account_id = pr_account_id,
	 			confirmed_document = pr_confirmed_document,
                reject_reason = pr_reject_reason,
	 			comment = pr_comment,
	 			approval_date = CURRENT_DATE()
	 		WHERE
	 			application_id = pr_application_id AND approver_cd = fn_get_system_code('staff_classification' , 'head_teacher');

	 		/* get real attend day */
	 		SET @real_attend_day = fn_get_real_attend_day(@person_id, pr_leaving_approval_date_from, pr_leaving_approval_date_to);

	 		/* clear comment 休学予定 in shift_attendance */
	 		SET @success = 0;
	 		CALL sp_update_comment_to_shift_attendance_by_person_id(@person_id, pr_leaving_date, pr_leaving_return_country_date, '', '休学予定', pr_account_id, @success);

			/* check update status */
			SELECT max(SA.is_authorized_absent), max(SA.is_leave_absent), max(SA.is_temporary_return)
			INTO
				@a1, @a2, @a3
			FROM
				shift_attendance SA INNER JOIN class_shift_work CSW ON SA.class_shift_work_id = CSW.id
			WHERE
				person_id = @person_id
				and SA.disable = 0
				and CSW.disable = 0
				and CSW.date in
					(SELECT date FROM school_calendar
					WHERE
						disable = 0
						AND is_school_holiday = 0
						AND is_public_holiday = 0
						AND date >= pr_leaving_approval_date_from
						AND date <= pr_leaving_approval_date_to);

			IF (@a1 > 0) OR (@a2 > 0) OR (@a3 > 0) THEN
				ROLLBACK;
				select 3 as error, @a1 as is_authorized_absent, @a2 as is_leave_absent, @a3 as is_temporary_return, 'data error' as errmsg;
				LEAVE update_app;
			END IF;


	 		/* set comment 休学 to shift_attendance */
	 		CALL sp_update_comment_to_shift_attendance_by_person_id(@person_id, pr_leaving_approval_date_from, pr_leaving_approval_date_to, '休学', '', pr_account_id, @success);

         /* set shift_attendance flag leave absent = 1 */
         UPDATE shift_attendance SA INNER JOIN class_shift_work CSW ON SA.class_shift_work_id = CSW.id
			SET SA.is_leave_absent = 1,
				SA.lastup_account_id = pr_account_id,
				SA.lastup_datetime = now()
			WHERE
				person_id = @person_id
				and SA.disable = 0
				and CSW.disable = 0
				and SA.comment = '休学'
				and CSW.date in (SELECT date FROM school_calendar WHERE disable = 0 AND is_school_holiday = 0 AND is_public_holiday = 0 AND date >= pr_leaving_approval_date_from AND date <= pr_leaving_approval_date_to)
			;

         /* update flag on attendance */
         UPDATE attendance A INNER JOIN class_shift_work CSW ON A.class_shift_work_id = CSW.id
			SET A.is_leave_absent = 1,
                 A.attendance = 0,
				A.lastup_account_id = pr_account_id,
				A.lastup_datetime = now()
			WHERE
				person_id = @person_id
				and A.disable = 0
				and CSW.disable = 0
				and CSW.date in (SELECT date FROM school_calendar WHERE disable = 0 AND is_school_holiday = 0 AND is_public_holiday = 0 AND date >= pr_leaving_approval_date_from AND date <= pr_leaving_approval_date_to)
			;

    END IF;

    IF `_rollback` THEN
        ROLLBACK;
        select 1 as error, 'exception error' as errmsg;
    ELSE
        COMMIT;
        select 0 as error;
    END IF;
END//
delimiter ;


