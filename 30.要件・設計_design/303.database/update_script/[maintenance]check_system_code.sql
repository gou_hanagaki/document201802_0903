﻿SELECT system_code_detail.system_code_id,system_code_detail.code1, system_code_detail.column_name, COUNT(*)
FROM system_code
INNER JOIN system_code_detail ON system_code.id = system_code_detail.system_code_id
WHERE system_code.`disable` = 0 AND system_code_detail.`disable` = 0
GROUP BY system_code_detail.system_code_id, system_code_detail.code1, system_code_detail.column_name
HAVING COUNT(*) > 1
