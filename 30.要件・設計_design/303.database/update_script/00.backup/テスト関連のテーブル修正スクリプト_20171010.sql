/* ability_groupのcolumn属性がおかしかったので修正 */
ALTER TABLE ability_group MODIFY id int(10) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE ability_group MODIFY name varchar(128) NOT NULL DEFAULT '';
ALTER TABLE ability_group MODIFY sequence int(10) unsigned NOT NULL DEFAULT '0';
ALTER TABLE ability_group MODIFY lastup_account_id int(10) unsigned NOT NULL DEFAULT '0';
ALTER TABLE ability_group MODIFY create_datetime datetime NOT NULL DEFAULT '0000-00-00 00:00:00';
ALTER TABLE ability_group MODIFY lastup_datetime datetime NOT NULL DEFAULT '0000-00-00 00:00:00';
ALTER TABLE ability_group MODIFY disable tinyint(1) unsigned NOT NULL DEFAULT '0';

/* drop table external_test */
DROP TABLE external_test;

/* create table external_test */
CREATE TABLE `external_test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `column_name` varchar(128) NOT NULL DEFAULT '',
  `is_for_student` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_for_staff` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `column_name` (`column_name`)
);

INSERT INTO `external_test` (`id`, `name`, `column_name`, `is_for_student`, `is_for_staff`, `sequence`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES
(1, '大学日本語教育課程主専攻', 'external_major_university_japanese_language',  0,  1,  1,  0,  '2017-09-26 01:16:28',  '2017-09-26 01:16:28',  0),
(2, '大学日本語教育課程副専攻', 'external_curriculum_university_japanese',  0,  1,  2,  0,  '2017-09-26 01:16:28',  '2017-09-26 01:16:28',  0),
(3, '日本語教育能力検定試験',  'external_jlpt_staff',  0,  1,  3,  0,  '2017-09-26 01:16:28',  '2017-09-26 01:16:28',  0),
(4, '日本語教師養成講座420時間', 'external_teacher_420_hours_japanese_language', 0,  1,  4,  0,  '2017-09-26 01:16:28',  '2017-09-26 01:16:28',  0),
(5, '大学卒業', 'external_university_graduation', 0,  1,  5,  0,  '2017-09-26 01:16:28',  '2017-09-26 01:16:28',  0),
(6, '日本語能力試験 JLPT', 'external_jlpt_student',  1,  0,  6,  0,  '2017-09-26 01:16:28',  '2017-09-26 01:16:28',  0),
(7, '日本留学試験 EJU', 'external_examination_eju', 1,  0,  7,  0,  '2017-09-26 01:16:28',  '2017-09-26 01:16:28',  0),
(8, '実用日本語検定 J.TEST', 'external_practical_japanese_test', 1,  0,  8,  0,  '2017-09-26 01:16:28',  '2017-09-26 01:16:28',  0),
(9, '実践日本語コミュニケーション検定 PJC Bridge', 'external_pjc_bridge',  1,  0,  9,  0,  '2017-09-26 01:16:28',  '2017-09-26 01:16:28',  0),
(10,  '申請等取次者証明書',  'external_certificate_of_applicant_etc',  0,  1,  10, 0,  '2017-09-26 01:16:28',  '2017-09-26 01:16:28',  0);


/* drop table external_test_level */
DROP TABLE external_test_level;

/* create table external_test_level */
CREATE TABLE `external_test_level` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `external_test_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `external_test_id` (`external_test_id`)
);

INSERT INTO `external_test_level` (`id`, `external_test_id`, `name`, `sequence`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES
(1, 1,  '', 0,  0,  '2017-09-26 01:25:00',  '2017-09-26 01:25:00',  0),
(2, 2,  '', 0,  0,  '2017-09-26 01:25:00',  '2017-09-26 01:25:00',  0),
(3, 3,  '', 0,  0,  '2017-09-26 01:25:00',  '2017-09-26 01:25:00',  0),
(4, 4,  '', 0,  0,  '2017-09-26 01:25:00',  '2017-09-26 01:25:00',  0),
(5, 5,  '', 0,  0,  '2017-09-26 01:25:00',  '2017-09-26 01:25:00',  0),
(6, 6,  'N1', 0,  0,  '2017-09-26 01:25:00',  '2017-09-26 01:25:00',  0),
(7, 6,  'N2', 0,  0,  '2017-09-26 01:25:00',  '2017-09-26 01:25:00',  0),
(8, 6,  'N3', 0,  0,  '2017-09-26 01:25:00',  '2017-09-26 01:25:00',  0),
(9, 6,  'N4', 0,  0,  '2017-09-26 01:25:00',  '2017-09-26 01:25:00',  0),
(10,  6,  'N5', 0,  0,  '2017-09-26 01:25:00',  '2017-09-26 01:25:00',  0),
(11,  7,  '', 0,  0,  '2017-09-26 01:25:00',  '2017-09-26 01:25:00',  0),
(12,  8,  'A',  0,  0,  '2017-09-26 01:25:00',  '2017-09-26 01:25:00',  0),
(13,  8,  'B',  0,  0,  '2017-09-26 01:25:00',  '2017-09-26 01:25:00',  0),
(14,  8,  'C',  0,  0,  '2017-09-26 01:25:00',  '2017-09-26 01:25:00',  0),
(15,  8,  'D',  0,  0,  '2017-09-26 01:25:00',  '2017-09-26 01:25:00',  0),
(16,  8,  'E',  0,  0,  '2017-09-26 01:25:00',  '2017-09-26 01:25:00',  0),
(17,  8,  'F',  0,  0,  '2017-09-26 01:25:00',  '2017-09-26 01:25:00',  0),
(18,  8,  'ビジネスJ.TEST', 0,  0,  '2017-09-26 01:25:00',  '2017-09-26 01:25:00',  0),
(19,  9,  '', 0,  0,  '2017-09-26 01:25:00',  '2017-09-26 01:25:00',  0),
(20,  10, '', 0,  0,  '2017-09-26 01:25:00',  '2017-09-26 01:25:00',  0);

/* alter table ability */
/* ★★external_testとexternal_test_levelのデータ移行が先★★ */
ALTER TABLE ability MODIFY `ability_group_id` int(10) unsigned NOT NULL DEFAULT '0' AFTER id;
ALTER TABLE ability CHANGE `test_level_id` `external_test_level_id` int(10) unsigned NOT NULL DEFAULT '0' AFTER ability_group_id;
ALTER TABLE ability CHANGE `next_ability` `next_ability_id` int(10) unsigned NOT NULL DEFAULT '0' AFTER external_test_level_id;

/* drop table test */
DROP TABLE test;
/* create table test */
CREATE TABLE `test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `column_name` varchar(128) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1)unsigned  NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `column_name` (`column_name`)
);
INSERT INTO `test` (`id`, `name`, `column_name`, `sequence`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES
(1, 'まとめテスト', 'summary_test', 0,  0,  '2017-09-26 11:37:54',  '2017-09-26 11:37:54',  0),
(2, '期末テスト',  'term_end_test',  0,  0,  '2017-09-26 11:37:54',  '2017-09-26 11:37:54',  0),
(3, 'プレースメントテスト', 'placement_test', 0,  0,  '2017-09-26 11:37:54',  '2017-09-26 11:37:54',  0),
(4, 'クラスアップテスト',  'class_up_test',  0,  0,  '2017-09-26 11:37:54',  '2017-09-26 11:37:54',  0),
(5, '日本語能力試験',  'term_end_jlpt_test', 0,  0,  '2017-09-30 18:42:46',  '2017-09-30 18:42:46',  0);

/* alter table content_test_list */
ALTER TABLE content_test_list ADD test_id int(10) unsigned NOT NULL DEFAULT '0' AFTER id;
ALTER TABLE content_test_list MODIFY `name` VARCHAR(256) NOT NULL DEFAULT '';
ALTER TABLE content_test_list ADD total_point int(10) unsigned NOT NULL DEFAULT '0' AFTER name;
ALTER TABLE content_test_list ADD threshold_rate int(7) unsigned NOT NULL DEFAULT '0' AFTER total_point;
ALTER TABLE content_test_list MODIFY time_test int(10) unsigned NOT NULL DEFAULT '0' AFTER threshold_rate;
ALTER TABLE content_test_list ADD public_type tinyint(1) unsigned NOT NULL DEFAULT '0' AFTER time_test;
ALTER TABLE content_test_list ADD sequence int(10) unsigned NOT NULL DEFAULT '0' AFTER public_type;
/* create table content_test_list */
/* dev環境以外の環境に新規でテーブル作成する時に使って下さい
CREATE TABLE `content_test_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `test_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ability_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(256) NOT NULL DEFAULT '',
  `total_point` int(10) unsigned NOT NULL DEFAULT '0',
  `threshold_rate` int(7) unsigned NOT NULL DEFAULT '0',
  `time_test` int(10) unsigned NOT NULL DEFAULT '0',
  `public_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ability_id` (`ability_id`)
);
*/

/* alter table content_test_question */
ALTER TABLE content_test_question CHANGE `test_list_id` `content_test_list_id` int(10) unsigned NOT NULL DEFAULT '0' AFTER id;
ALTER TABLE content_test_question ADD test_section_cd int(3) unsigned NOT NULL DEFAULT '0' AFTER content_test_list_id;
ALTER TABLE content_test_question CHANGE `question_sec` `question_sequence` int(10) unsigned NOT NULL DEFAULT '0' AFTER test_kind;
ALTER TABLE content_test_question ADD `score_kind` tinyint(1) unsigned NOT NULL DEFAULT '0' AFTER score;
ALTER TABLE content_test_question MODIFY `question` TEXT NOT NULL AFTER score_kind;
ALTER TABLE content_test_question DROP COLUMN `test_section_id`;
ALTER TABLE content_test_question DROP COLUMN `is_answer`;
/* create table content_test_question */
/* dev環境以外の環境に新規でテーブル作成する時に使って下さい
CREATE TABLE `content_test_question` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_list_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_section_cd` int(3) unsigned NOT NULL DEFAULT '0',
  `test_title` text NOT NULL,
  `test_kind` varchar(50) NOT NULL DEFAULT '',
  `question_sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `score` int(10) unsigned NOT NULL DEFAULT '0',
  `score_kind` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:all, 1: one',
  `question` text NOT NULL,
  `hint` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `test_list_id` (`content_test_list_id`)
);
*/

/* alter table content_test_answer */
ALTER TABLE content_test_answer CHANGE `sequence` `answer_sequence` int(10) unsigned NOT NULL DEFAULT '0' AFTER content_test_question_id;
ALTER TABLE content_test_answer CHANGE `answer_sec` `answer_selection` varchar(50) NOT NULL DEFAULT '' AFTER hint;
/* create table content_test_answer */
/* dev環境以外の環境に新規でテーブル作成する時に使って下さい
CREATE TABLE `content_test_answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_question_id` int(10) unsigned NOT NULL DEFAULT '0',
  `answer_sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `answer` text NOT NULL,
  `hint` text NOT NULL,
  `answer_selection` varchar(50) NOT NULL DEFAULT '',
  `is_correct` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_test_question_id` (`content_test_question_id`)
)
*/

/* drop table achievement_detail */
DROP TABLE achievement;

/* create table achievement */
CREATE TABLE `achievement` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `content_test_list_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_date` date NOT NULL DEFAULT '0000-00-00',
  `total_score` int(10) unsigned NOT NULL DEFAULT '0',
  `is_additional_test` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `content_test_list_id` (`content_test_list_id`),
  KEY `class_id` (`class_id`),
  KEY `test_date` (`test_date`)
);
/* drop table achievement_detail */
DROP TABLE achievement_detail;

/* create table achievement_detail */
CREATE TABLE `achievement_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `achievement_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_section_cd` int(3) unsigned NOT NULL DEFAULT '0',
  `score` int(10) unsigned NOT NULL DEFAULT '0',
  `score_original` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `achievement_id` (`achievement_id`),
  KEY `test_section_cd` (`test_section_cd`)
);

/* drop table content_test_status */
DROP TABLE content_test_status;

/* create table content_test_status */
CREATE TABLE `content_test_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_list_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `total_score` int(10) unsigned NOT NULL DEFAULT '0',
  `status` int(3) unsigned NOT NULL DEFAULT '0',
  `is_teacher` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_test_list_id` (`content_test_list_id`),
  KEY `class_id` (`class_id`),
  KEY `person_id` (`person_id`)
);

/* drop table content_test_section */
DROP TABLE content_test_section;

/* create table content_test_section */
CREATE TABLE `content_test_section` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_list_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_section_cd` int(3) unsigned NOT NULL DEFAULT '0',
  `total_point` int(10) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_test_list_id` (`content_test_list_id`),
  KEY `test_section_cd` (`test_section_cd`)
);

/* alter table examination_answer */
ALTER TABLE examination_answer ADD test_section_cd int(3) unsigned NOT NULL DEFAULT '0' AFTER achievement_id;
ALTER TABLE examination_answer CHANGE `answer_sec` `answer_position` int(10) unsigned NOT NULL DEFAULT '0' AFTER test_section_cd;
ALTER TABLE examination_answer CHANGE `selected_content_test_answer_id` `content_test_answer_id` int(10) unsigned NOT NULL DEFAULT '0' AFTER answer_comment;
ALTER TABLE examination_answer MODIFY `test_kind` VARCHAR(50) NOT NULL DEFAULT '' AFTER score;
/* create table examination_answer */
/* dev環境以外の環境に新規でテーブル作成する時に使って下さい
CREATE TABLE `examination_answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_question_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `achievement_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_section_cd` int(3) unsigned NOT NULL DEFAULT '0',
  `answer_position` int(10) unsigned NOT NULL DEFAULT '0',
  `answer_comment` text NOT NULL,
  `content_test_answer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `score` varchar(10) NOT NULL DEFAULT '',
  `test_kind` varchar(50) NOT NULL DEFAULT '',
  `is_correct` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_test_question_id` (`content_test_question_id`),
  KEY `person_id` (`person_id`),
  KEY `achievement_id` (`achievement_id`),
  KEY `selected_content_test_answer_id` (`content_test_answer_id`)
);
*/

/* drop table test_level */
DROP TABLE test_level;

/* drop table class_activity */
DROP TABLE class_activity;




/* INSERT system_code + system_code_datail */
INSERT INTO system_code SET name = '試験科目', column_name = 'test_section', create_datetime = now(), lastup_datetime=now();
SET @ver_id =  LAST_INSERT_ID();
INSERT INTO system_code_detail SET system_code_id = @ver_id, code1 = 1, name = '文字語彙', create_datetime = now(), lastup_datetime = now();
INSERT INTO system_code_detail SET system_code_id = @ver_id, code1 = 2, name = '文法', create_datetime = now(), lastup_datetime = now();
INSERT INTO system_code_detail SET system_code_id = @ver_id, code1 = 3, name = '文作', create_datetime = now(), lastup_datetime = now();
INSERT INTO system_code_detail SET system_code_id = @ver_id, code1 = 4, name = '読解', create_datetime = now(), lastup_datetime = now();
INSERT INTO system_code_detail SET system_code_id = @ver_id, code1 = 5, name = '聴解', create_datetime = now(), lastup_datetime = now();


ALTER TABLE class_shift_request_level
CHANGE test_level_id external_test_level_id int(10) unsigned NOT NULL DEFAULT '0';

/* drop table class_activity */
DROP TABLE content_test_answer_selection;

CREATE TABLE `content_test_answer_selection` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_question_id` int(10) unsigned NOT NULL DEFAULT '0',
  `selection_sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `hint` text NOT NULL,
  `answer_selection` varchar(50) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_test_question_id` (`content_test_question_id`)
);

ALTER TABLE content_test_answer DROP COLUMN hint;
ALTER TABLE content_test_answer DROP COLUMN answer_selection;

/* 20170930 */
ALTER TABLE staff_ability DROP COLUMN ability_id;




-- set column_name for external_test
UPDATE `external_test` SET `column_name`='external_major_university_japanese_language' WHERE  `id`=1;
UPDATE `external_test` SET `column_name`='external_curriculum_university_japanese' WHERE  `id`=2;
UPDATE `external_test` SET `column_name`='external_jlpt_staff' WHERE  `id`=3;
UPDATE `external_test` SET `column_name`='external_teacher_420_hours_japanese_language' WHERE  `id`=4;
UPDATE `external_test` SET `column_name`='external_university_graduation' WHERE  `id`=5;
UPDATE `external_test` SET `column_name`='external_jlpt_student' WHERE  `id`=6;
UPDATE `external_test` SET `column_name`='external_examination_eju' WHERE  `id`=7;
UPDATE `external_test` SET `column_name`='external_practical_japanese_test' WHERE  `id`=8;
UPDATE `external_test` SET `column_name`='external_pjc_bridge' WHERE  `id`=9;
UPDATE `external_test` SET `column_name`='external_certificate_of_applicant_etc' WHERE  `id`=10;

-- Set column_name test table
UPDATE `test` SET `column_name`='summary_test' WHERE  `id`=1;
UPDATE `test` SET `column_name`='term_end_test' WHERE  `id`=2;
UPDATE `test` SET `column_name`='placement_test' WHERE  `id`=3;
UPDATE `test` SET `column_name`='class_up_test' WHERE  `id`=4;
UPDATE `test` SET `column_name`='term_end_jlpt_test' WHERE  `id`=5;

-- Set column_name system_code table
UPDATE `system_code_detail` SET `name`='文字語彙文法' WHERE  `id`=483;
UPDATE `system_code_detail` SET `column_name`='term_end_test_vocabulary' WHERE  `id`=481;
UPDATE `system_code_detail` SET `column_name`='term_end_test_grammar' WHERE  `id`=482;
UPDATE `system_code_detail` SET `column_name`='jlpt_vocabulary_grammar' WHERE  `id`=483;
UPDATE `system_code_detail` SET `column_name`='jlpt_reading' WHERE  `id`=484;
UPDATE `system_code_detail` SET `column_name`='jlpt_listening' WHERE  `id`=485;
INSERT INTO `system_code_detail` ( `system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES (467, 6, 0, 0, 0, '文作', 'term_middle_test_vocabulary', 0, '2017-09-26 10:09:36', '2017-09-26 10:09:36', 0);
INSERT INTO `system_code_detail` ( `system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES (467, 7, 0, 0, 0, 'Test section 1', 'term_middle_test_grammar', 0, '2017-09-26 10:09:36', '2017-09-26 10:09:36', 0);


CREATE TABLE IF NOT EXISTS `staff_qualification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(10) unsigned NOT NULL DEFAULT '0',
  `external_test_level_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `staff_id` (`staff_id`),
  KEY `external_test_level_id` (`external_test_level_id`),
  KEY `lastup_account_id` (`lastup_account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

alter table application modify `temporary_return_flight_name_go` varchar(255) NOT NULL DEFAULT '';
alter table application modify `temporary_return_flight_name_comeback` varchar(255) NOT NULL DEFAULT '';
alter table application modify `temporary_return_address2` varchar(255) NOT NULL DEFAULT '';
alter table application modify `temporary_return_address3` varchar(255) NOT NULL DEFAULT '';
alter table application modify `leave_absence_return_country_mail_address` varchar(255) NOT NULL DEFAULT '';
alter table application modify `leave_absence_return_country_address1` varchar(255) NOT NULL DEFAULT '';
alter table application modify `leave_absence_return_country_address2` varchar(255) NOT NULL DEFAULT '';
alter table application modify `leave_absence_return_country_address3` varchar(255) NOT NULL DEFAULT '';
alter table application modify `leave_absence_reason_id` int(10) unsigned NOT NULL DEFAULT '0';


