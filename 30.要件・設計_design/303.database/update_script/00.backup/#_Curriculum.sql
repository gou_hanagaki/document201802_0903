-- https://redmine.lampart-vn.com/issues/23406
-- -----------------------------------------------------
-- Table `class_shift`
-- -----------------------------------------------------
ALTER TABLE `class_shift`
ADD COLUMN `is_optional` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' AFTER `teacher_id`;

-- -----------------------------------------------------
-- Table `class_group_unit`
-- -----------------------------------------------------
ALTER TABLE `class_group_unit`
ADD COLUMN `comment4` text NOT NULL AFTER `is_confirmed`;

ALTER TABLE `class_group_unit`
ADD COLUMN `comment3` text NOT NULL AFTER `is_confirmed`;

ALTER TABLE `class_group_unit`
ADD COLUMN `comment2` text NOT NULL AFTER `is_confirmed`;

ALTER TABLE `class_group_unit`
ADD COLUMN `comment1` text NOT NULL AFTER `is_confirmed`;

