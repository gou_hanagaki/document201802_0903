-- -----------------------------------------------------
-- Table `application_official_absence_properties`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `application_official_absence_properties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `application_id` int(10) unsigned NOT NULL DEFAULT '0',
  `authorized_absent_date_from` DATE NOT NULL DEFAULT '0000-00-00',
  `authorized_absent_date_to` DATE NOT NULL DEFAULT '0000-00-00',
  `confirmed_absent_date_from` DATE NOT NULL DEFAULT '0000-00-00',
  `confirmed_absent_date_to` DATE NOT NULL DEFAULT '0000-00-00',
  `authorized_absent_reason_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `authorized_absent_school_name` varchar(128) NOT NULL DEFAULT '',
  `authorized_absent_school_section` varchar(128) NOT NULL DEFAULT '',
  `authorized_absent_professor` varchar(128) NOT NULL DEFAULT '',
  `authorized_absent_ceremony_relation` varchar(128) NOT NULL DEFAULT '',
  `authorized_absent_company` varchar(128) NOT NULL DEFAULT '',
  `authorized_absent_comment` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`)
) COLLATE='utf8_general_ci' ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;