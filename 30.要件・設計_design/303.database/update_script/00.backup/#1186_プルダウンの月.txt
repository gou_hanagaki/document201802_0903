﻿INSERT INTO `system_code` (`name`, `column_name`, `lastup_account_id`, `create_datetime`) VALUES ('月', 'month', '2000', now());

SET @id := LAST_INSERT_ID();

INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`) VALUES (@id, '1', '1', '0', '1', '1月 ', 'january', '2000', now());
INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`) VALUES (@id, '2', '1', '0', '2', '2月', 'february', '2000', now());
INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`) VALUES (@id, '3', '1', '0', '3', '3月', 'march', '2000', now());
INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`) VALUES (@id, '4', '4', '0', '4', '4月', 'april', '2000', now());
INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`) VALUES (@id, '5', '4', '0', '5', '5月', 'may', '2000', now());
INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`) VALUES (@id, '6', '4', '0', '6', '6月', 'june', '2000', now());
INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`) VALUES (@id, '7', '7', '0', '7', '7月 ', 'july', '2000', now());
INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`) VALUES (@id, '8', '7', '0', '8', '8月', 'august', '2000', now());
INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`) VALUES (@id, '9', '7', '0', '9', '9月', 'september', '2000', now());
INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`) VALUES (@id, '10', '10', '0', '10', '10月', 'october', '2000', now());
INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`) VALUES (@id, '11', '10', '0', '11', '11月', 'november', '2000', now());
INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `code2`, `code3`, `sequence`, `name`, `column_name`, `lastup_account_id`, `create_datetime`) VALUES (@id, '12', '10', '0', '12', '12月', 'december', '2000', now());
