﻿-- function_group
INSERT INTO `function_group` (`id`, `name`, `sequence`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES
(11,	'カリキュラム',	11,	2000,	'2017-10-04 00:00:00',	'0000-00-00 00:00:00',	0);

-- function
INSERT INTO `function` (`function_group_id`, `name`, `path`, `is_in_menu`, `sequence`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES
(11,	'クラス分トップ画面',	'staff/curriculum/student_classify_top',	1,	6,	2000,	'2017-10-04 00:00:00',	'0000-00-00 00:00:00',	0),
(11,	'未クラス分け学生画面',	'staff/curriculum/student_classify_new',	0,	1,	2000,	'2017-10-04 00:00:00',	'0000-00-00 00:00:00',	0),
(11,	'各クラス学生画面',	'staff/curriculum/student_classify_edit',	0,	2,	2000,	'2017-10-04 00:00:00',	'0000-00-00 00:00:00',	0),
(11,	'クラス編成画面',	'staff/curriculum/bulk_class_create',	1,	3,	2000,	'2017-10-04 00:00:00',	'0000-00-00 00:00:00',	0),
(11,	'クラス追加画面',	'staff/curriculum/class_create',	0,	2,	2000,	'2017-10-04 00:00:00',	'0000-00-00 00:00:00',	0),
(11,	'シフト作成画面',	'staff/curriculum/create_class_shift',	1,	5,	2000,	'2017-10-04 00:00:00',	'0000-00-00 00:00:00',	0),
(11,	'シフト希望画面',	'staff/curriculum/shift_request_list',	1,	3,	2000,	'2017-10-04 00:00:00',	'0000-00-00 00:00:00',	0),
(11,	'シフト希望詳細画面',	'staff/curriculum/shift_request_detail',	0,	4,	2000,	'2017-10-04 00:00:00',	'0000-00-00 00:00:00',	0),
(11,	'授業スケジュール',	'staff/curriculum/schedule',	0,	9,	2000,	'2017-10-04 00:00:00',	'0000-00-00 00:00:00',	0),
(11,	'授業スケジュール配布用画面',	'staff/curriculum/schedule_for_distribution',	0,	10,	2000,	'2017-10-04 00:00:00',	'0000-00-00 00:00:00',	0),
(11,	'レベル一覧画面',	'staff/curriculum/level_list',	1,	0,	2000,	'2017-10-04 00:00:00',	'0000-00-00 00:00:00',	0);

INSERT INTO `permission` (`account_group_id`, `function_group_id`, `is_permitted`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES
(1,	11,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0),
(2,	11,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0),
(3,	11,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0),
(4,	11,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0),
(5,	11,	0,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0),
(6,	11,	1,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0),
(7,	11,	1,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0),
(8,	11,	1,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0),
(9,	11,	1,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0),
(10,	11,	1,	0,	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	0);


