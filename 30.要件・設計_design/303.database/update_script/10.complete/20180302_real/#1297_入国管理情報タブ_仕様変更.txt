ALTER TABLE `visa` 
CHANGE COLUMN `visa_length_of_stay` `visa_length_of_stay` VARCHAR(128) NOT NULL DEFAULT '';

Set @id=(select id from system_code where system_code.column_name='residence_application_result');

INSERT INTO `system_code_detail` (`system_code_id`,`code1`,`code2`,`code3`,`sequence`,`name`,`column_name`,`lastup_account_id`,`create_datetime`) VALUES 
(@id, '0', '0', '0', '0', '���\��', 'unapplied', '2000', now());

UPDATE system_code_detail
LEFT JOIN system_code ON system_code.id = system_code_id
SET system_code_detail.disable=1, system_code_detail.lastup_account_id=2000, system_code_detail.lastup_datetime=now() 
WHERE system_code.id=@id and system_code_detail.column_name='Withdrawal';
