-- Insert certificate_submit_to
INSERT INTO `system_code` (`name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) VALUES ('提出先', 'certificate_submit_to',0 , now(),now());
SET @system_code_id = LAST_INSERT_ID();
INSERT INTO `system_code_detail` (`system_code_id`, `code1` , `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) VALUES (@system_code_id, 1, '入国管理局', 'immigration_bureau',0 , now(),now());
INSERT INTO `system_code_detail` (`system_code_id`, `code1` , `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) VALUES (@system_code_id, 2, '大学', 'university',0 , now(),now());
INSERT INTO `system_code_detail` (`system_code_id`, `code1` , `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) VALUES (@system_code_id, 3, '大学院', 'graduate_school',0 , now(),now());
INSERT INTO `system_code_detail` (`system_code_id`, `code1` , `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) VALUES (@system_code_id, 4, '専門学校', 'vocational_school',0 , now(),now());
INSERT INTO `system_code_detail` (`system_code_id`, `code1` , `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) VALUES (@system_code_id, 5, '会社', 'company',0 , now(),now());
INSERT INTO `system_code_detail` (`system_code_id`, `code1` , `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) VALUES (@system_code_id, 6, 'アルバイト先', 'part_time_job',0 , now(),now());
INSERT INTO `system_code_detail` (`system_code_id`, `code1` , `name`, `column_name`, `lastup_account_id`, `create_datetime`, `lastup_datetime`) VALUES (@system_code_id, 7, 'その他', 'other',0 , now(),now());

-- Disable reason_for_not_issuing_letter_of_recommendation cause pending
update application_type set disable = 1 where application_type.column_name = 'reason_for_not_issuing_letter_of_recommendation';
