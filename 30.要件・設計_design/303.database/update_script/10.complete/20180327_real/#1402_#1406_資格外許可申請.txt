-- -----------------------------------------------------
-- Table `immigration`
-- -----------------------------------------------------
ALTER TABLE `immigration`
ADD COLUMN `other_activity_expiration_date` date NOT NULL DEFAULT '0000-00-00' AFTER `other_activity_is_outputted`;

INSERT INTO `pdf_function` (`name`, `link`) VALUES ('���i�O���\��', '/staff/admission/permission_engage_other_activity;');

INSERT INTO `function` 
(`function_group_id`, `name`, `path`, `is_in_menu`, `sequence`, `lastup_account_id`, `create_datetime`) 
VALUES
(2,	'���i�O�������\��',	'staff/admission/permission_engage_other_activity	',	1,	11,	2000,	now());