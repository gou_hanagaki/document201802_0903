-- -----------------------------------------------------
-- INSERT new system_code`
-- -----------------------------------------------------
INSERT INTO `system_code` (`name`, `column_name`,`lastup_account_id`,`create_datetime`,`lastup_datetime`) 
VALUES('帰国する場合', 'leaving_case', 0, now(), now()) ;

SELECT @id := LAST_INSERT_ID();

INSERT INTO `system_code_detail` (`system_code_id`, `code1`, `name`, `column_name`,`lastup_account_id`,`create_datetime`,`lastup_datetime`)
VALUES
(@id, '1', '帰国', 'return_home', 0, now(), now()),
(@id, '2', '日本国内の高等教育機関に進学', 'study_in_higher', 0, now(), now()),
(@id, '3', '日本国内の企業に就職', 'work_in_jp', 0, now(), now()),
(@id, '4', 'その他の在留資格へ変更', 'changing_to_other', 0, now(), now());

-- -----------------------------------------------------
-- Table `application_issue_certificate_properties`
-- -----------------------------------------------------
CREATE TABLE `application_withdrawal_notification_properties` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `application_id` INT(10) UNSIGNED NOT NULL DEFAULT 0 ,
  `leaving_desire_date` DATE NOT NULL DEFAULT '0000-00-00',
  `leaving_return_country_date` DATE NOT NULL DEFAULT '0000-00-00',
  `leaving_date` DATE NOT NULL DEFAULT '0000-00-00',
  `leaving_return_country_flight` VARCHAR (255) NOT NULL DEFAULT '',
  `leaving_return_country_phone_no` VARCHAR (20) NOT NULL DEFAULT '',
  `leaving_return_country_mail_address` VARCHAR (255) NOT NULL DEFAULT '',
  `leaving_case` TINYINT (1) UNSIGNED NOT NULL DEFAULT 0,
  `lastup_account_id` INT (10) UNSIGNED NOT NULL DEFAULT 0,
  `create_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` TINYINT (1) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`), INDEX (application_id)
)  COLLATE='utf8_general_ci' ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

