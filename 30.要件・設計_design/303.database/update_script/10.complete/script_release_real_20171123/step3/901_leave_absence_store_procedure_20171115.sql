SET NAMES utf8;
DROP FUNCTION IF EXISTS `fn_get_id_by_column_name`;
delimiter //
CREATE FUNCTION `fn_get_id_by_column_name`(
  `pr_system_code_name` VARCHAR(100),
  `pr_system_code_detail_name` VARCHAR(100)

)
  RETURNS int(11)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
  SQL SECURITY DEFINER
  COMMENT ''
  BEGIN
    SET @id = (SELECT id FROM application_type WHERE column_name = pr_system_code_detail_name);
    IF @id IS NULL THEN
      SET @id = 0;
    END IF;
    RETURN @id;
  END//
delimiter ;


SET NAMES utf8;
DROP FUNCTION IF EXISTS `fn_get_real_attend_day`;
delimiter //
CREATE FUNCTION `fn_get_real_attend_day`(
	`pr_person_id` INT,
	`pr_from_date` DATE,
	`pr_to_date` DATE
)
RETURNS int(11)
LANGUAGE SQL
DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'get real attend day from date to date'
BEGIN
  IF (pr_from_date > pr_to_date) THEN
    SET @real_attend = -1;
  ELSE
    SET @real_attend = (SELECT COUNT(date)
                FROM school_calendar
                WHERE
                  disable = 0
                  AND is_school_holiday = 0
                  AND is_public_holiday = 0
                  AND date >= pr_from_date
                  AND date <= pr_to_date);
	END IF;
	RETURN (@real_attend);
END//
delimiter ;



SET NAMES utf8;
DROP FUNCTION IF EXISTS `fn_get_system_code`;
delimiter //
CREATE FUNCTION `fn_get_system_code`(
	`pr_system_code_name` VARCHAR(100),
	`pr_system_code_detail_name` VARCHAR(100)

)
RETURNS int(11)
LANGUAGE SQL
DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'get system code detail'
BEGIN
  SET @value = (SELECT
            SCD.code1
          FROM
            system_code_detail SCD INNER JOIN system_code SC ON SCD.system_code_id = SC.id
          WHERE
            SC.column_name = pr_system_code_name
            AND SCD.column_name = pr_system_code_detail_name);
  IF (@value IS NULL) THEN
    RETURN 0;
  ELSE
    RETURN @value;
  END IF;
END//
delimiter ;


SET NAMES utf8;
DROP PROCEDURE IF EXISTS `sp_get_application_leave_absence_info`;
delimiter //
CREATE PROCEDURE `sp_get_application_leave_absence_info`(
	IN `pr_application_id` INT

)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'Get application leave absence info'
BEGIN
	 SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');
	 SET @full_time_teacher = fn_get_system_code('staff_classification','full-time_faculty_staff');
	 SET @part_time_teacher = fn_get_system_code('staff_classification','part-time_faculty_staff');
	 SET @special_teacher = fn_get_system_code('staff_classification','special_lecturer');

	 # Get teacher approval info
	 SELECT
		 	application_approver_id, approval_date
		 INTO
		 	@teacher_id, @teacher_approval_date
		 FROM
		 	application_approve
		 WHERE
		 	application_id = pr_application_id
			 AND (approver_cd = @full_time_teacher OR approver_cd = @part_time_teacher OR approver_cd = @special_teacher)
	 	LIMIT 1;

	 SET @teacher_name = (SELECT CONCAT(last_name, ' ', first_name) as full_name FROM person WHERE id = @teacher_id);

    SELECT
        A.application_no,
        A.application_type_id,
        A.application_date,
        A.status_cd,
        ALAP.leave_absence_date_from as request_leaving_date_from,
        ALAP.leave_absence_date_to as request_leaving_date_to,
        ALAP.leave_absence_approval_date_from as approval_leaving_date_from,
        ALAP.leave_absence_approval_date_to as approval_leaving_date_to,
        ALAP.leave_absence_return_country_phone_no as leaving_return_country_phone_no,
        ALAP.leave_absence_return_country_mail_address as leaving_return_country_mail_address,
        ALAP.leave_absence_return_country_address1 as leaving_return_country_address1,
        ALAP.leave_absence_return_country_address2 as leaving_return_country_address2,
        ALAP.leave_absence_return_country_address3 as leaving_return_country_address3,
        ALAP.leave_absence_reason_id as reason_absence,
        ALAP.leave_absence_reason_detail as reason_absence_detail,
        ALAP.leave_absence_note as school_type_note,
        #A.leave_absence_teacher_confirm_date,

        A2.g7_approval_date,
        A2.g7_confirmed_document as confirmed_document_secretary_general,
        A2.g7_reject_reason as reject_reason_secretary_general,
        A2.g7_comment as comment_secretary_general,
        A2.g7_status_cd as status_cd_secretary_general,

        A3.g6_approval_date,
        A3.g6_confirmed_document as confirmed_document_college_chief,
        A3.g6_reject_reason as reject_reason_college_chief,
        A3.g6_comment as comment_college_chief,
        A3.g6_status_cd status_cd_college_chief,

        A4.g9_approval_date,
        A4.g9_confirmed_document as confirmed_document_head_school,
        A4.g9_reject_reason as reject_reason_head_school,
        A4.g9_comment as comment_head_school,
        A4.g9_status_cd as status_cd_head_school,

        @teacher_approval_date as leave_absence_teacher_confirm_date,
        @teacher_name as leave_absence_teacher_confirm_name,
        @teacher_id as teacher_id

	 FROM
        application A
        INNER JOIN application_leave_absence_properties ALAP ON A.id = ALAP.application_id
        LEFT JOIN (SELECT
                application_id,
                approval_date as g7_approval_date,
                confirmed_document as g7_confirmed_document,
                reject_reason as g7_reject_reason,
                comment as g7_comment,
                status_cd as g7_status_cd
            FROM application_approve
            WHERE application_id = pr_application_id AND approver_cd = fn_get_system_code('staff_classification','ceo') and disable = 0) A2 ON A.id = A2.application_id
        LEFT JOIN (SELECT
                application_id,
                approval_date as g6_approval_date,
                confirmed_document as g6_confirmed_document,
                reject_reason as g6_reject_reason,
                comment as g6_comment,
                status_cd as g6_status_cd
            FROM application_approve
            WHERE application_id = pr_application_id AND approver_cd = fn_get_system_code('staff_classification','education_chief') and disable = 0) A3 ON A.id = A3.application_id
        LEFT JOIN (SELECT
                application_id,
                approval_date as g9_approval_date,
                confirmed_document as g9_confirmed_document,
                reject_reason as g9_reject_reason,
                comment as g9_comment,
                status_cd as g9_status_cd
            FROM application_approve
            WHERE application_id = pr_application_id AND approver_cd = fn_get_system_code('staff_classification','head_teacher') and disable = 0) A4 ON A.id = A4.application_id
    WHERE A.id = pr_application_id and A.application_type_id = @app_type_id and A.disable = 0;
END//
delimiter ;



SET NAMES utf8;
DROP PROCEDURE IF EXISTS `sp_get_real_attend_by_student_no`;
delimiter //
CREATE PROCEDURE `sp_get_real_attend_by_student_no`(
	IN `pr_student_no` VARCHAR(128),
	IN `pr_from_date` DATE,
	IN `pr_to_date` DATE

)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'Get real attend of current class'
get_attend:BEGIN
	SET @person_id = (SELECT person_id FROM student WHERE student_no = pr_student_no);
	IF @person_id IS NULL THEN
		SELECT 1 as error, 'student not found' as errmsg;
		LEAVE get_attend;
	END IF;

	SET @real_attend = fn_get_real_attend_day(pr_student_no, pr_from_date, pr_to_date);
	IF @real_attend > 0 THEN
		/* count attendance data */
		SET @attend_count = (SELECT
										count(DISTINCT CSW.date)
									FROM
										shift_attendance SA INNER JOIN class_shift_work CSW ON SA.class_shift_work_id = CSW.id
									WHERE
										SA.person_id = @person_id
										and CSW.date >= pr_from_date
										and CSW.date <= pr_to_date);
		/* check attendance valid */
		IF @real_attend > @attend_count THEN
			SELECT 2 as error, 'attendance data not found' as errmsg;
			LEAVE get_attend;
		END IF;

		/* valid result */
		SELECT 0 as error,  @real_attend as real_attend_day;
	ELSE
		SELECT 2 as error, 'attendance data not found' as errmsg;
	END IF;
END//
delimiter ;



SET NAMES utf8;
DROP PROCEDURE IF EXISTS `sp_get_student_info`;
delimiter //
CREATE PROCEDURE `sp_get_student_info`(
	IN `param_student_no` VARCHAR(128)

)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'Get current info of student'
BEGIN
    SET @person_id = (SELECT person_id FROM student WHERE student_no = param_student_no);
    /* get current class */
    SET @class_id = (SELECT class_id FROM class_student WHERE person_id = @person_id  AND date_from <= CURRENT_DATE() AND date_to >= CURRENT_DATE() LIMIT 1);
    SET @class_name = (SELECT name FROM class WHERE id = @class_id);
    SET @teacher_person_id = (SELECT teacher_person_id FROM class WHERE id = @class_id);
    SET @teacher_account_id = (SELECT id FROM account WHERE person_id = @teacher_person_id);
    SET @teacher_name = (SELECT CONCAT(last_name, ' ', first_name) as full_name FROM person WHERE id = @teacher_person_id);
    SET @total_leave_absent = (SELECT count(DISTINCT CSW.date) FROM shift_attendance SA INNER JOIN class_shift_work CSW ON SA.class_shift_work_id = CSW.id WHERE person_id = @person_id and SA.is_leave_absent = 1);

    SELECT
        CONCAT(P.last_name, ' ', P.first_name) as full_name,
        CONCAT(P.last_name_kana, ' ', P.first_name_kana) as full_name_kana,
        P.sex_cd,
        C.name as county_name,
        C.english_name as english_county_name,
        P.phone_no,
        P.phone_no2,
        P.mobile_no,
        P.zipcode,
        P.address1,
        P.address2,
        P.address3,
        P.mail_address1,
        P.mail_address2,
        I.home_country_phone_no1,
        I.home_country_phone_no2,
        I.home_country_address1,
        I.home_country_address2,
        I.home_country_address3,
        A.attend_day,
        A.absent_day,
        A.total_day,
        A.rate_time,
        @class_id as class_id,
        @class_name as class_name,
        @teacher_person_id as teacher_person_id,
        @teacher_name as teacher_name,
        @person_id as person_id,
        @total_leave_absent as total_leave_absent,
        @teacher_account_id as teacher_account_id
    FROM
        person P
        LEFT JOIN country C ON P.country_id = C.id
        INNER JOIN student S on P.id = S.person_id
        LEFT JOIN immigration I on P.id = I.person_id
        LEFT JOIN (SELECT
        						 rate_time,
                         student_id,
                         attend_day,
                         absent_day,
                         total_day
                     FROM attendance_rate WHERE student_id=@person_id and attendance_cd = 1) A ON P.id = A.student_id
    WHERE
        P.id = @person_id;
END//
delimiter ;



SET NAMES utf8;
DROP PROCEDURE IF EXISTS `sp_get_total_absent`;
delimiter //
CREATE PROCEDURE `sp_get_total_absent`(
	IN `param_student_no` VARCHAR(128)
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'Get total absent of student'
BEGIN
    SET @student_id = (select person_id from student where student_no = param_student_no);
    SELECT absent_day
        FROM attendance_rate
        WHERE disable = 0
            AND student_id = @student_id
            AND attendance_cd = 1;
END//
delimiter ;



SET NAMES utf8;
DROP PROCEDURE IF EXISTS `sp_insert_leave_absence_application`;
delimiter //
CREATE PROCEDURE `sp_insert_leave_absence_application`(
	IN `pr_student_no` VARCHAR(128)
    ,
	IN `pr_application_date` DATE
    ,
	IN `pr_charge_id` INT
    ,
	IN `pr_leaving_date` DATE
    ,
	IN `pr_leaving_return_country_date` DATE
    ,
	IN `pr_leaving_return_country_phone_no` VARCHAR(20)
    ,
	IN `pr_leaving_return_country_mail_address` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address1` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address2` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address3` VARCHAR(255)
    ,
	IN `pr_leaving_reason_id` INT,
	IN `pr_lastup_account_id` INT
    ,
	IN `pr_leaving_reason_detail` TEXT
    ,
	IN `pr_leaving_approval_date_from` VARCHAR(50),
	IN `pr_leaving_approval_date_to` VARCHAR(50),
	IN `pr_leaving_note` TEXT

)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'Insert new leaving application'
SET NAMES utf8;
DROP PROCEDURE IF EXISTS `sp_insert_leave_absence_application`;
delimiter //
CREATE PROCEDURE `sp_insert_leave_absence_application`(
	IN `pr_student_no` VARCHAR(128)
    ,
	IN `pr_application_date` DATE
    ,
	IN `pr_charge_id` INT
    ,
	IN `pr_leaving_date` DATE
    ,
	IN `pr_leaving_return_country_date` DATE
    ,
	IN `pr_leaving_return_country_phone_no` VARCHAR(20)
    ,
	IN `pr_leaving_return_country_mail_address` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address1` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address2` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address3` VARCHAR(255)
    ,
	IN `pr_leaving_reason_id` INT,
	IN `pr_lastup_account_id` INT
    ,
	IN `pr_leaving_reason_detail` TEXT
    ,
	IN `pr_leaving_approval_date_from` VARCHAR(50),
	IN `pr_leaving_approval_date_to` VARCHAR(50),
	IN `pr_leaving_note` TEXT

)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'Insert new leaving application'
BEGIN
    DECLARE `_rollback` BOOL DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
    START TRANSACTION;
	     # Get applitcation type id
	     SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');

        # get request_no
        SET @person_id = (SELECT person_id FROM student WHERE student_no = pr_student_no);
        SET @str_request_no = (select max(request_no) from request);
        IF (@str_request_no IS NULL) THEN
            SET @str_request_no = 1;
        END IF;
        SET @request_no = CAST(@str_request_no AS SIGNED) + 1;
        SET @str_request_no = RIGHT(CONCAT('00000000', CAST(@request_no AS CHAR(8))), 8);

        # Insert new request
        INSERT INTO request(request_no, person_id, lastup_account_id, lastup_datetime, create_datetime, disable)
            VALUES(
                @str_request_no, @person_id, pr_lastup_account_id, now(), now(), 0
            );
        SET @last_request_id = LAST_INSERT_ID();

        # get application_no
        /*
        SET @str_application_no = (select max(application_no) from application);
        IF (@str_application_no IS NULL) THEN
            SET @str_application_no = 1;
        END IF;
        SET @application_no = CAST(@str_application_no AS SIGNED) + 1;
        SET @str_application_no = RIGHT(CONCAT('00000000', CAST(@application_no AS CHAR(8))), 8);
        */
        # Insert new applicaton record
        INSERT INTO application(
                    request_id,
                    #application_no,
                    application_type_id,
                    application_date,
                    status_cd,
                    charge_id,
                    lastup_account_id,
                    create_datetime,
                    lastup_datetime
                )
               VALUES(
                    @last_request_id,
                    #@str_application_no,
                    @app_type_id,
                    pr_application_date,
                    1,
                    pr_charge_id,
                    pr_lastup_account_id,
                    now(),
                    now()
                );
			SET @last_application_id = LAST_INSERT_ID();

		# Insert new application_leave_absence record
		INSERT INTO application_leave_absence_properties(
						  lastup_account_id,
                    create_datetime,
                    lastup_datetime,
                    application_id,
		              leave_absence_reason_id,
                    leave_absence_date_from,
                    leave_absence_date_to,
                    leave_absence_return_country_phone_no,
                    leave_absence_return_country_mail_address,
                    leave_absence_return_country_address1,
                    leave_absence_return_country_address2,
                    leave_absence_return_country_address3,
                    leave_absence_reason_detail,
                    leave_absence_approval_date_from,
                    leave_absence_approval_date_to,
                    leave_absence_note
                )
               VALUES(
                    pr_lastup_account_id,
                    now(),
                    now(),
                    @last_application_id,
                    pr_leaving_reason_id,
                    pr_leaving_date,
                    pr_leaving_return_country_date,
                    pr_leaving_return_country_phone_no,
                    pr_leaving_return_country_mail_address,
                    pr_leaving_return_country_address1,
                    pr_leaving_return_country_address2,
                    pr_leaving_return_country_address3,
                    pr_leaving_reason_detail,
                    pr_leaving_approval_date_from,
                    pr_leaving_approval_date_to,
                    pr_leaving_note
                );

		# Insert new application_approve
		INSERT INTO application_approve(application_id, approver_cd, action, comment, lastup_account_id, create_datetime, lastup_datetime) VALUES
			(@last_application_id, fn_get_system_code('staff_classification','ceo'), '', '', pr_lastup_account_id, now(), now()),
			(@last_application_id, fn_get_system_code('staff_classification','education_chief'), '', '', pr_lastup_account_id, now(), now()),
			(@last_application_id, fn_get_system_code('staff_classification','head_teacher'), '', '', pr_lastup_account_id, now(), now());

		# Insert teacher confirm data
		/*
		SET @class_id = (SELECT class_id FROM class_student WHERE person_id = @person_id  AND date_from <= CURRENT_DATE() AND date_to >= CURRENT_DATE() LIMIT 1);
	   SET @teacher_person_id = (SELECT teacher_person_id FROM class WHERE id = @class_id);
		INSERT INTO application_approve(application_id, application_approver_id, approver_cd, action, comment, lastup_account_id, create_datetime, lastup_datetime)
			SELECT
				@last_application_id,
				@teacher_person_id,
				P.`type`,
				'',
				'',
				pr_lastup_account_id,
				now(),
				now()
			FROM
				person P
			WHERE
				P.id = @teacher_person_id;
	  */

    # update comment shift_attendance
    SET @success = 0;
    CALL sp_update_comment_to_shift_attendance_by_person_id(@person_id, pr_leaving_date, pr_leaving_return_country_date, '休学予定', '', pr_lastup_account_id, @success);

    IF (`_rollback`) OR (@success = 0) OR (@app_type_id = 0) THEN
        ROLLBACK;
        ALTER TABLE request auto_increment = 1;
        ALTER TABLE application auto_increment = 1;
        ALTER TABLE application_approve auto_increment = 1;
        select 2 as error, _rollback, @success, @app_type_id, @last_application_id,  @str_request_no ;
    ELSE
        COMMIT;
        select 0 as error;
    END IF;
END//
delimiter ;



SET NAMES utf8;
DROP PROCEDURE IF EXISTS `sp_leave_absence_application_cancel`;
delimiter //
CREATE PROCEDURE `sp_leave_absence_application_cancel`(
	IN `pr_application_id` INT
,
	IN `pr_lastup_account_id` INT
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'Cancel leave absence application'
cancel_leaving_app:BEGIN
    DECLARE `_rollback` BOOL DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
	 /* get application type by column name */
	 SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');

    SET @status_cd = (SELECT status_cd FROM application WHERE id = pr_application_id and application_type_id = @app_type_id and disable = 0);
    SET @secretary_general_remand = fn_get_system_code('application_status', 'secretary_general_remand');
    SET @secretariat_input = fn_get_system_code('application_status', 'secretariat_input');

    /* check exists */
    IF @status_cd IS NULL THEN
        SELECT 1 as error, 'application not found' as errmsg;
        LEAVE cancel_leaving_app;
    END IF;

    SET @success = 1;
    START TRANSACTION;
    /* check status = 8 or 1*/
    IF (@status_cd <> @secretary_general_remand) AND (@status_cd <> @secretariat_input) THEN
        SELECT 2 as error, 'Cancel not yet ready' as errmsg;
        COMMIT;
        LEAVE cancel_leaving_app;
    ELSE
            /* disable application */
            SET @request_id = (select request_id from application where id = pr_application_id);
            SET @person_id = (select person_id from request where id = @request_id);
            SET @from_date = (select leave_absence_date_from from application_leave_absence_properties where application_id = pr_application_id);
            SET @to_date = (select leave_absence_date_to from application_leave_absence_properties where application_id = pr_application_id);

            UPDATE application
            SET
                status_cd = fn_get_system_code('application_status', 'secretariat_remand'),
                lastup_account_id = pr_lastup_account_id,
                lastup_datetime = now()
            WHERE
                id = pr_application_id and (status_cd = @secretary_general_remand OR status_cd = @secretariat_input) and disable = 0;

            /* update comment shift_attendance */
            CALL sp_update_comment_to_shift_attendance_by_person_id(@person_id, @from_date, @to_date, '', '休学予定', pr_lastup_account_id, @success);

    END IF;

    IF (`_rollback`) OR (@success = 0) THEN
        ROLLBACK;
        select 1 as error, 'exception error' as errmsg;
    ELSE
        COMMIT;
        select 0 as error;
    END IF;
END//
delimiter ;



SET NAMES utf8;
DROP PROCEDURE IF EXISTS `sp_leave_absence_application_group6_approval`;
delimiter //
CREATE PROCEDURE `sp_leave_absence_application_group6_approval`(
	IN `pr_application_id` INT
    ,
	IN `pr_confirmed_document` VARCHAR(255)
    ,
	IN `pr_reject_reason` VARCHAR(255)
    ,
	IN `pr_comment` VARCHAR(255)
    ,
	IN `pr_application_date` DATE
    ,
	IN `pr_leaving_date` DATE
    ,
	IN `pr_leaving_return_country_date` DATE
    ,
	IN `pr_leaving_approval_date_from` VARCHAR(50),
	IN `pr_leaving_approval_date_to` VARCHAR(50),
	IN `pr_leaving_return_country_phone_no` VARCHAR(20)
    ,
	IN `pr_leaving_return_country_mail_address` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address1` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address2` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address3` VARCHAR(255),
	IN `pr_leaving_reason` TEXT
    ,
	IN `pr_leaving_reason_detail` TEXT
    ,
	IN `pr_leaving_note` TEXT
    ,
	IN `pr_account_id` INT
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'Education Chief approval leave absence application'
update_app:BEGIN
	DECLARE `_rollback` BOOL DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
	START TRANSACTION;
	/* get application type by column name */
	 SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');
	 SET @secretary_general_approved = fn_get_system_code('application_status' , 'secretary_general_approved');
	 SET @school_chief_remand = fn_get_system_code('application_status' , 'school_chief_remand');

    SET @status_cd = (SELECT status_cd FROM application WHERE id = pr_application_id and application_type_id = @app_type_id and disable = 0);
    /* check exists */
    IF @status_cd IS NULL THEN
        SELECT 1 as error, 'application not found' as errmsg;
        LEAVE update_app;
    END IF;

    /* check status = 2 or 6 */
    IF (@status_cd <> @secretary_general_approved) and (@status_cd <> @school_chief_remand) THEN
        SELECT 2 as error, 'Secretary-General not yet confirm' as errmsg;
        LEAVE update_app;
    ELSE

    		/* update application status to 3 */
    		UPDATE application
    		SET
    			status_cd = fn_get_system_code('application_status' , 'education_chief_approved'),
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
    			application_date = pr_application_date
    		WHERE
    			id = pr_application_id;

    		/* Update application properties */
    		UPDATE application_leave_absence_properties
    		SET
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
				leave_absence_date_from = pr_leaving_date,
             leave_absence_date_to = pr_leaving_return_country_date,
             leave_absence_return_country_phone_no = pr_leaving_return_country_phone_no,
             leave_absence_return_country_mail_address = pr_leaving_return_country_mail_address,
             leave_absence_return_country_address1 = pr_leaving_return_country_address1,
             leave_absence_return_country_address2 = pr_leaving_return_country_address2,
             leave_absence_return_country_address3 = pr_leaving_return_country_address3,
             leave_absence_reason_id = pr_leaving_reason,
             leave_absence_reason_detail = pr_leaving_reason_detail,
             leave_absence_approval_date_from = pr_leaving_approval_date_from,
             leave_absence_approval_date_to = pr_leaving_approval_date_to,
             leave_absence_note = pr_leaving_note
			WHERE application_id = pr_application_id;


    		/* update application_approve data */
	 		UPDATE
	 			application_approve
	 		SET
	 			application_approver_id = pr_account_id,
	 			status_cd = 1,
	 			lastup_datetime = now(),
	 			lastup_account_id = pr_account_id,
	 			confirmed_document = pr_confirmed_document,
            reject_reason = pr_reject_reason,
	 			comment = pr_comment,
	 			approval_date = CURRENT_DATE()
	 		WHERE
	 			application_id = pr_application_id AND approver_cd = fn_get_system_code('staff_classification', 'education_chief');
    END IF;

    IF `_rollback` THEN
        ROLLBACK;
        select 1 as error;
    ELSE
        COMMIT;
        select 0 as error;
    END IF;
END//
delimiter ;



SET NAMES utf8;
DROP PROCEDURE IF EXISTS `sp_leave_absence_application_group6_reject`;
delimiter //
CREATE PROCEDURE `sp_leave_absence_application_group6_reject`(
	IN `pr_application_id` INT
    ,
	IN `pr_confirmed_document` VARCHAR(255)
    ,
	IN `pr_reject_reason` VARCHAR(255)
    ,
	IN `pr_comment` VARCHAR(255)
    ,
	IN `pr_application_date` DATE
    ,
	IN `pr_leaving_date` DATE
    ,
	IN `pr_leaving_return_country_date` DATE
    ,
	IN `pr_leaving_approval_date_from` VARCHAR(50),
	IN `pr_leaving_approval_date_to` VARCHAR(50),
	IN `pr_leaving_return_country_phone_no` VARCHAR(20)
    ,
	IN `pr_leaving_return_country_mail_address` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address1` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address2` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address3` VARCHAR(255),
	IN `pr_leaving_reason` TEXT
    ,
	IN `pr_leaving_reason_detail` TEXT
    ,
	IN `pr_leaving_note` TEXT
    ,
	IN `pr_account_id` INT
)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'Education Chief reject leave absence application '
update_app:BEGIN
	DECLARE `_rollback` BOOL DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
	START TRANSACTION;
	/* get application type by column name */
	 SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');
	 SET @secretary_general_approved = fn_get_system_code('application_status' , 'secretary_general_approved');
	 SET @school_chief_remand = fn_get_system_code('application_status' , 'school_chief_remand');

    SET @status_cd = (SELECT status_cd FROM application WHERE id = pr_application_id and application_type_id = @app_type_id and disable = 0);
    /* check exists */
    IF @status_cd IS NULL THEN
        SELECT 1 as error, 'application not found' as errmsg;
        LEAVE update_app;
    END IF;

    /* check status = 2 or 6 */
    IF (@status_cd <> @secretary_general_approved) and (@status_cd <> @school_chief_remand) THEN
        SELECT 2 as error, 'Secretary General not yet confirm' as errmsg;
        LEAVE update_app;
    ELSE

    		/* update application status to 7 */
    		UPDATE application
    		SET
    			status_cd = fn_get_system_code('application_status' , 'education_chief_remand'),
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
            application_date = pr_application_date
    		WHERE
    			id = pr_application_id;

    		/* Update application properties */
    		UPDATE application_leave_absence_properties
    		SET
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
				leave_absence_date_from = pr_leaving_date,
             leave_absence_date_to = pr_leaving_return_country_date,
             leave_absence_return_country_phone_no = pr_leaving_return_country_phone_no,
             leave_absence_return_country_mail_address = pr_leaving_return_country_mail_address,
             leave_absence_return_country_address1 = pr_leaving_return_country_address1,
             leave_absence_return_country_address2 = pr_leaving_return_country_address2,
             leave_absence_return_country_address3 = pr_leaving_return_country_address3,
             leave_absence_reason_id = pr_leaving_reason,
             leave_absence_reason_detail = pr_leaving_reason_detail,
             leave_absence_approval_date_from = pr_leaving_approval_date_from,
             leave_absence_approval_date_to = pr_leaving_approval_date_to,
             leave_absence_note = pr_leaving_note
			WHERE application_id = pr_application_id;

    		/* update application_approve data */
	 		UPDATE
	 			application_approve
	 		SET
	 			application_approver_id = pr_account_id,
	 			status_cd = 2,
	 			lastup_datetime = now(),
	 			lastup_account_id = pr_account_id,
            confirmed_document = pr_confirmed_document,
	 			reject_reason = pr_reject_reason,
	 			comment = pr_comment,
            approval_date = CURRENT_DATE()
	 		WHERE
	 			application_id = pr_application_id AND approver_cd = fn_get_system_code('staff_classification', 'education_chief');
    END IF;

    IF `_rollback` THEN
        ROLLBACK;
        select 1 as error;
    ELSE
        COMMIT;
        select 0 as error;
    END IF;
END//
delimiter ;



SET NAMES utf8;
DROP PROCEDURE IF EXISTS `sp_leave_absence_application_group7_approval`;
delimiter //
CREATE PROCEDURE `sp_leave_absence_application_group7_approval`(
	IN `pr_application_id` INT
    ,
	IN `pr_confirmed_document` VARCHAR(255)
    ,
	IN `pr_reject_reason` VARCHAR(255)
    ,
	IN `pr_comment` VARCHAR(255)
    ,
	IN `pr_application_date` DATE
    ,
	IN `pr_leaving_date` DATE
    ,
	IN `pr_leaving_return_country_date` DATE
    ,
	IN `pr_leaving_approval_date_from` VARCHAR(50),
	IN `pr_leaving_approval_date_to` VARCHAR(50),
	IN `pr_leaving_return_country_phone_no` VARCHAR(20)
    ,
	IN `pr_leaving_return_country_mail_address` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address1` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address2` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address3` VARCHAR(255),
	IN `pr_leaving_reason` TEXT
    ,
	IN `pr_leaving_reason_detail` TEXT
    ,
	IN `pr_leaving_note` TEXT
    ,
	IN `pr_account_id` INT

)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'Ceo approval leave absence application '
update_app:BEGIN
	DECLARE `_rollback` BOOL DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
	START TRANSACTION;
		/* get application type by column name */
	 SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');
	 SET @secretariat_input = fn_get_system_code('application_status' , 'secretariat_input');
	 SET @education_chief_remand = fn_get_system_code('application_status' , 'education_chief_remand');

    SET @status_cd = (SELECT status_cd FROM application WHERE id = pr_application_id and application_type_id = @app_type_id and disable = 0);
    /* check exists */
    IF @status_cd IS NULL THEN
        SELECT 1 as error, 'application not found' as errmsg;
        LEAVE update_app;
    END IF;

    /* check status = 1 OR 7 */
    IF (@status_cd <> @secretariat_input) AND (@status_cd <> @education_chief_remand) THEN
        SELECT 2 as error, 'flow approval error' as errmsg;
        LEAVE update_app;
    ELSE

    		/* update application status to 2 */
    		UPDATE application
    		SET
    			status_cd = fn_get_system_code('application_status' , 'secretary_general_approved'),
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
             application_date = pr_application_date
    		WHERE
    			id = pr_application_id;

    		/* Update application properties */
    		UPDATE application_leave_absence_properties
    		SET
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
				leave_absence_date_from = pr_leaving_date,
             leave_absence_date_to = pr_leaving_return_country_date,
             leave_absence_return_country_phone_no = pr_leaving_return_country_phone_no,
             leave_absence_return_country_mail_address = pr_leaving_return_country_mail_address,
             leave_absence_return_country_address1 = pr_leaving_return_country_address1,
             leave_absence_return_country_address2 = pr_leaving_return_country_address2,
             leave_absence_return_country_address3 = pr_leaving_return_country_address3,
             leave_absence_reason_id = pr_leaving_reason,
             leave_absence_reason_detail = pr_leaving_reason_detail,
             leave_absence_approval_date_from = pr_leaving_approval_date_from,
             leave_absence_approval_date_to = pr_leaving_approval_date_to,
             leave_absence_note = pr_leaving_note
			WHERE application_id = pr_application_id;

    		/* update application_approve data */
	 		UPDATE
	 			application_approve
	 		SET
	 			application_approver_id = pr_account_id,
	 			status_cd = 1,
	 			lastup_datetime = now(),
	 			lastup_account_id = pr_account_id,
	 			confirmed_document = pr_confirmed_document,
                reject_reason = pr_reject_reason,
	 			comment = pr_comment,
	 			approval_date = CURRENT_DATE()
	 		WHERE
	 			application_id = pr_application_id AND approver_cd = fn_get_system_code('staff_classification' , 'ceo');
    END IF;

    IF `_rollback` THEN
        ROLLBACK;
        select 1 as error;
    ELSE
        COMMIT;
        select 0 as error;
    END IF;
END//
delimiter ;



SET NAMES utf8;
DROP PROCEDURE IF EXISTS `sp_leave_absence_application_group7_reject`;
delimiter //
CREATE PROCEDURE `sp_leave_absence_application_group7_reject`(
	IN `pr_application_id` INT
    ,
	IN `pr_confirmed_document` VARCHAR(255)
    ,
	IN `pr_reject_reason` VARCHAR(255)
    ,
	IN `pr_comment` VARCHAR(255)
    ,
	IN `pr_application_date` DATE
    ,
	IN `pr_leaving_date` DATE
    ,
	IN `pr_leaving_return_country_date` DATE
    ,
	IN `pr_leaving_approval_date_from` VARCHAR(50),
	IN `pr_leaving_approval_date_to` VARCHAR(50),
	IN `pr_leaving_return_country_phone_no` VARCHAR(20)
    ,
	IN `pr_leaving_return_country_mail_address` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address1` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address2` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address3` VARCHAR(255),
	IN `pr_leaving_reason` TEXT
    ,
	IN `pr_leaving_reason_detail` TEXT
    ,
	IN `pr_leaving_note` TEXT
    ,
	IN `pr_account_id` INT

)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'Ceo reject leave absence application '
update_app:BEGIN
	DECLARE `_rollback` BOOL DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
	START TRANSACTION;
	/* get application type by column name */
	 SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');
	 SET @secretariat_input = fn_get_system_code('application_status' , 'secretariat_input');
	 SET @education_chief_remand = fn_get_system_code('application_status' , 'education_chief_remand');

    SET @status_cd = (SELECT status_cd FROM application WHERE id = pr_application_id and application_type_id = @app_type_id and disable = 0);
    /* check exists */
    IF @status_cd IS NULL THEN
        SELECT 1 as error, 'application not found' as errmsg;
        LEAVE update_app;
    END IF;

    /* check status = 1 or 7*/
    IF (@status_cd <> @secretariat_input) AND (@status_cd <> @education_chief_remand) THEN
        SELECT 2 as error, 'Status not yet reject' as errmsg;
        LEAVE update_app;
    ELSE

    		/* update application status to 8 */
    		UPDATE application
    		SET
    			status_cd = fn_get_system_code('application_status' , 'secretary_general_remand'),
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
                application_date = pr_application_date
    		WHERE
    			id = pr_application_id;

    		/* Update application properties */
    		UPDATE application_leave_absence_properties
    		SET
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
				leave_absence_date_from = pr_leaving_date,
             leave_absence_date_to = pr_leaving_return_country_date,
             leave_absence_return_country_phone_no = pr_leaving_return_country_phone_no,
             leave_absence_return_country_mail_address = pr_leaving_return_country_mail_address,
             leave_absence_return_country_address1 = pr_leaving_return_country_address1,
             leave_absence_return_country_address2 = pr_leaving_return_country_address2,
             leave_absence_return_country_address3 = pr_leaving_return_country_address3,
             leave_absence_reason_id = pr_leaving_reason,
             leave_absence_reason_detail = pr_leaving_reason_detail,
             leave_absence_approval_date_from = pr_leaving_approval_date_from,
             leave_absence_approval_date_to = pr_leaving_approval_date_to,
             leave_absence_note = pr_leaving_note
			WHERE application_id = pr_application_id;

    		/* update application_approve data */
	 		UPDATE
	 			application_approve
	 		SET
	 			application_approver_id = pr_account_id,
	 			status_cd = 2,
	 			lastup_datetime = now(),
	 			lastup_account_id = pr_account_id,
                confirmed_document = pr_confirmed_document,
	 			reject_reason = pr_reject_reason,
	 			comment = pr_comment,
                approval_date = CURRENT_DATE()
	 		WHERE
	 			application_id = pr_application_id AND approver_cd = fn_get_system_code('staff_classification' , 'ceo');
    END IF;

    IF `_rollback` THEN
        ROLLBACK;
        select 1 as error;
    ELSE
        COMMIT;
        select 0 as error;
    END IF;
END//
delimiter ;



SET NAMES utf8;
DROP PROCEDURE IF EXISTS `sp_leave_absence_application_group9_approval`;
delimiter //
CREATE PROCEDURE `sp_leave_absence_application_group9_approval`(
	IN `pr_application_id` INT
    ,
	IN `pr_confirmed_document` VARCHAR(255)
    ,
	IN `pr_reject_reason` VARCHAR(255)
    ,
	IN `pr_comment` VARCHAR(255)
    ,
	IN `pr_application_date` DATE
    ,
	IN `pr_leaving_date` DATE
    ,
	IN `pr_leaving_return_country_date` DATE
    ,
	IN `pr_leaving_approval_date_from` DATE,
	IN `pr_leaving_approval_date_to` DATE,
	IN `pr_leaving_return_country_phone_no` VARCHAR(20)
    ,
	IN `pr_leaving_return_country_mail_address` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address1` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address2` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address3` VARCHAR(255),
	IN `pr_leaving_reason` TEXT
    ,
	IN `pr_leaving_reason_detail` TEXT
    ,
	IN `pr_leaving_note` TEXT
    ,
	IN `pr_account_id` INT

)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'Head of School approval leave absence application '
update_app:BEGIN
	DECLARE `_rollback` BOOL DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
	/* get application type by column name */
	 SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');
	 SET @education_chief_approved = fn_get_system_code('application_status' , 'education_chief_approved');


    SET @status_cd = (SELECT status_cd FROM application WHERE id = pr_application_id and application_type_id = @app_type_id and disable = 0);
    /* check exists */
    IF @status_cd IS NULL THEN
        SELECT 1 as error, 'application not found' as errmsg;
        LEAVE update_app;
    END IF;



    START TRANSACTION;
    /* check status = 3 */
    IF @status_cd <> @education_chief_approved THEN
    		COMMIT;
        SELECT 2 as error, 'Chief not yet confirm' as errmsg;
        LEAVE update_app;
    ELSE
    		 /* get person_id */
		    SET @request_id = (SELECT request_id FROM application WHERE id = pr_application_id);
		    SET @person_id = (SELECT person_id FROM request WHERE id = @request_id);

    		/* update application status to 4 */

    		UPDATE application
    		SET
    			status_cd = fn_get_system_code('application_status' , 'school_chief_approved'),
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
                application_date = pr_application_date
    		WHERE
    			id = pr_application_id;

    		/* Update application properties */
    		UPDATE application_leave_absence_properties
    		SET
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
				leave_absence_date_from = pr_leaving_date,
             leave_absence_date_to = pr_leaving_return_country_date,
             leave_absence_return_country_phone_no = pr_leaving_return_country_phone_no,
             leave_absence_return_country_mail_address = pr_leaving_return_country_mail_address,
             leave_absence_return_country_address1 = pr_leaving_return_country_address1,
             leave_absence_return_country_address2 = pr_leaving_return_country_address2,
             leave_absence_return_country_address3 = pr_leaving_return_country_address3,
             leave_absence_reason_id = pr_leaving_reason,
             leave_absence_reason_detail = pr_leaving_reason_detail,
             leave_absence_approval_date_from = pr_leaving_approval_date_from,
             leave_absence_approval_date_to = pr_leaving_approval_date_to,
             leave_absence_note = pr_leaving_note
			WHERE application_id = pr_application_id;


    		/* update application_approve data */
	 		UPDATE
	 			application_approve
	 		SET
	 			application_approver_id = pr_account_id,
	 			status_cd = 1,
	 			lastup_datetime = now(),
	 			lastup_account_id = pr_account_id,
	 			confirmed_document = pr_confirmed_document,
                reject_reason = pr_reject_reason,
	 			comment = pr_comment,
	 			approval_date = CURRENT_DATE()
	 		WHERE
	 			application_id = pr_application_id AND approver_cd = fn_get_system_code('staff_classification' , 'head_teacher');

	 		/* get real attend day */
	 		SET @real_attend_day = fn_get_real_attend_day(@person_id, pr_leaving_approval_date_from, pr_leaving_approval_date_to);




	 		/* clear comment 休学予定 in shift_attendance */
	 		SET @success = 0;
	 		CALL sp_update_comment_to_shift_attendance_by_person_id(@person_id, pr_leaving_date, pr_leaving_return_country_date, '', '休学予定', pr_account_id, @success);

			/* check update status */
			SELECT max(SA.is_authorized_absent), max(SA.is_leave_absent), max(SA.is_temporary_return)
			INTO
				@a1, @a2, @a3
			FROM
				shift_attendance SA INNER JOIN class_shift_work CSW ON SA.class_shift_work_id = CSW.id
			WHERE
				person_id = @person_id
				and SA.disable = 0
				and CSW.disable = 0
				and CSW.date in
					(SELECT date FROM school_calendar
					WHERE
						disable = 0
						AND is_school_holiday = 0
						AND is_public_holiday = 0
						AND date >= pr_leaving_approval_date_from
						AND date <= pr_leaving_approval_date_to);

			IF (@a1 > 0) OR (@a2 > 0) OR (@a3 > 0) THEN
				ROLLBACK;
				select 3 as error, @a1 as is_authorized_absent, @a2 as is_leave_absent, @a3 as is_temporary_return, 'data error' as errmsg;
				LEAVE update_app;
			END IF;


	 		/* set comment 休学 to shift_attendance */
	 		CALL sp_update_comment_to_shift_attendance_by_person_id(@person_id, pr_leaving_approval_date_from, pr_leaving_approval_date_to, '休学', '', pr_account_id, @success);

         /* set shift_attendance flag leave absent = 1 */
         UPDATE shift_attendance SA INNER JOIN class_shift_work CSW ON SA.class_shift_work_id = CSW.id
			SET SA.is_leave_absent = 1,
				SA.lastup_account_id = pr_account_id,
				SA.lastup_datetime = now()
			WHERE
				person_id = @person_id
				and SA.disable = 0
				and CSW.disable = 0
				and SA.comment = '休学'
				and CSW.date in (SELECT date FROM school_calendar WHERE disable = 0 AND is_school_holiday = 0 AND is_public_holiday = 0 AND date >= pr_leaving_approval_date_from AND date <= pr_leaving_approval_date_to)
			;

         /* update flag on attendance */
         UPDATE attendance A INNER JOIN class_shift_work CSW ON A.class_shift_work_id = CSW.id
			SET A.is_leave_absent = 1,
                 A.attendance = 0,
				A.lastup_account_id = pr_account_id,
				A.lastup_datetime = now()
			WHERE
				person_id = @person_id
				and A.disable = 0
				and CSW.disable = 0
				and CSW.date in (SELECT date FROM school_calendar WHERE disable = 0 AND is_school_holiday = 0 AND is_public_holiday = 0 AND date >= pr_leaving_approval_date_from AND date <= pr_leaving_approval_date_to)
			;

    END IF;

    IF `_rollback` THEN
        ROLLBACK;
        select 1 as error, 'exception error' as errmsg;
    ELSE
        COMMIT;
        select 0 as error;
    END IF;
END//
delimiter ;



SET NAMES utf8;
DROP PROCEDURE IF EXISTS `sp_leave_absence_application_group9_reject`;
delimiter //
CREATE PROCEDURE `sp_leave_absence_application_group9_reject`(
	IN `pr_application_id` INT
    ,
	IN `pr_confirmed_document` VARCHAR(255)
    ,
	IN `pr_reject_reason` VARCHAR(255)
    ,
	IN `pr_comment` VARCHAR(255)
    ,
	IN `pr_application_date` DATE
    ,
	IN `pr_leaving_date` DATE
    ,
	IN `pr_leaving_return_country_date` DATE
    ,
	IN `pr_leaving_approval_date_from` VARCHAR(50),
	IN `pr_leaving_approval_date_to` VARCHAR(50),
	IN `pr_leaving_return_country_phone_no` VARCHAR(20)
    ,
	IN `pr_leaving_return_country_mail_address` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address1` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address2` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address3` VARCHAR(255),
	IN `pr_leaving_reason` TEXT
    ,
	IN `pr_leaving_reason_detail` TEXT
    ,
	IN `pr_leaving_note` TEXT
    ,
	IN `pr_account_id` INT

)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'Head of School approval leave absence application '
update_app:BEGIN
	DECLARE `_rollback` BOOL DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
	START TRANSACTION;
	/* get application type by column name */
	 SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');
	 SET @education_chief_approved = fn_get_system_code('application_status' , 'education_chief_approved');

    SET @status_cd = (SELECT status_cd FROM application WHERE id = pr_application_id and application_type_id = @app_type_id and disable = 0);
    /* check exists */
    IF @status_cd IS NULL THEN
        SELECT 1 as error, 'application not found' as errmsg;
        LEAVE update_app;
    END IF;

    /* check status = 3 */
    IF @status_cd <> @education_chief_approved THEN
        SELECT 2 as error, 'College Chief not yet confirm' as errmsg;
        LEAVE update_app;
    ELSE

    		/* update application status to 6 */
    		UPDATE application
    		SET
    			status_cd = fn_get_system_code('application_status' , 'school_chief_remand'),
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
                application_date = pr_application_date
    		WHERE
    			id = pr_application_id;

    		/* Update application properties */
    		UPDATE application_leave_absence_properties
    		SET
    			lastup_account_id = pr_account_id,
    			lastup_datetime = now(),
				leave_absence_date_from = pr_leaving_date,
             leave_absence_date_to = pr_leaving_return_country_date,
             leave_absence_return_country_phone_no = pr_leaving_return_country_phone_no,
             leave_absence_return_country_mail_address = pr_leaving_return_country_mail_address,
             leave_absence_return_country_address1 = pr_leaving_return_country_address1,
             leave_absence_return_country_address2 = pr_leaving_return_country_address2,
             leave_absence_return_country_address3 = pr_leaving_return_country_address3,
             leave_absence_reason_id = pr_leaving_reason,
             leave_absence_reason_detail = pr_leaving_reason_detail,
             leave_absence_approval_date_from = pr_leaving_approval_date_from,
             leave_absence_approval_date_to = pr_leaving_approval_date_to,
             leave_absence_note = pr_leaving_note
			WHERE application_id = pr_application_id;

    		/* update application_approve data */
	 		UPDATE
	 			application_approve
	 		SET
	 			application_approver_id = pr_account_id,
	 			status_cd = 2,
	 			lastup_datetime = now(),
	 			lastup_account_id = pr_account_id,
                confirmed_document = pr_confirmed_document,
	 			reject_reason = pr_reject_reason,
	 			comment = pr_comment,
                approval_date = CURRENT_DATE()
	 		WHERE
	 			application_id = pr_application_id AND approver_cd = fn_get_system_code('staff_classification' , 'head_teacher');
    END IF;

    IF `_rollback` THEN
        ROLLBACK;
        select 1 as error;
    ELSE
        COMMIT;
        select 0 as error;
    END IF;
END//
delimiter ;



SET NAMES utf8;
DROP PROCEDURE IF EXISTS `sp_leave_absence_application_teacher_confirm`;
delimiter //
CREATE PROCEDURE `sp_leave_absence_application_teacher_confirm`(
	IN `pr_application_id` INT
,
	IN `pr_account_id` INT

)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'Teacher confirm leave absence form'
teacher_confirm:BEGIN
	DECLARE `_rollback` BOOL DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
	/* get application type by column name */
	 SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');

	/* check application exists */
	SET @id = (SELECT id FROM application WHERE id = pr_application_id AND disable = 0 AND application_type_id = @app_type_id);
	IF @id IS NULL THEN
		SELECT 1 as error, 'application not found' as errmsg;
		LEAVE teacher_confirm;
	END IF;

	/* get teacher id */
	SET @request_id = (SELECT request_id FROM application WHERE id = pr_application_id AND disable = 0 AND application_type_id = @app_type_id);
	SET @student_person_id = (SELECT person_id FROM request WHERE id = @request_id);
	SET @class_id = (SELECT class_id FROM class_student WHERE person_id = @student_person_id  AND date_from <= CURRENT_DATE() AND date_to >= CURRENT_DATE() LIMIT 1);
   SET @class_name = (SELECT name FROM class WHERE id = @class_id);
   SET @teacher_person_id = (SELECT teacher_person_id FROM class WHERE id = @class_id);
   SET @teacher_person_type = (SELECT `type` FROM person WHERE id = @teacher_person_id);
   #SET @teacher_account_id = (SELECT id FROM account WHERE person_id = @teacher_person_id);

	/* Check teacher person id */
	IF @teacher_person_id IS NULL THEN
		SELECT 2 as error, 'teacher not found' as errmsg;
		LEAVE teacher_confirm;
	END IF;

	/* Check teacher confirm record */
	SET @check_confirmed = (SELECT 1 FROM application_approve WHERE application_id = @id
										AND (approver_cd = fn_get_system_code('staff_classification', 'full-time_faculty_staff')
												OR approver_cd = fn_get_system_code('staff_classification', 'part-time_faculty_staff')
												OR approver_cd = fn_get_system_code('staff_classification', 'special_lecturer')
												)
									);
	IF @check_confirmed = 1 THEN
		SELECT 3 as error, 'teacher confirmed' as errmsg;
		LEAVE teacher_confirm;
	END IF;

	/* INSERT CONFIRM RECORD */

	START TRANSACTION;
	INSERT INTO application_approve(
		application_id,
		application_approver_id,
		approver_cd,
		status_cd,
		action,
		interview_id,
		approval_date,
		confirmed_document,
		reject_reason,
		comment,
		lastup_account_id,
		create_datetime,
		lastup_datetime)
	VALUES(
		@id,
		@teacher_person_id,
		@teacher_person_type,
		1,
		'',
		0,
		CURRENT_DATE(),
		'',
		'',
		'',
		pr_account_id,
		now(),
		now()
	);

	/* update teacher confirm date */
	/*
	UPDATE application_approve
	SET
		application_approver_id = pr_account_id,
		approval_date = now(),
		status_cd = 1
	WHERE
		application_id = pr_application_id
		AND application_approver_id = pr_account_id
		AND (approver_cd = fn_get_system_code('staff_classification', 'full-time_faculty_staff')
			OR approver_cd = fn_get_system_code('staff_classification', 'part-time_faculty_staff')
			OR approver_cd = fn_get_system_code('staff_classification', 'special_lecturer')
		)
	;
	*/

	IF `_rollback` THEN
        ROLLBACK;
        select 1 as error;
    ELSE
        COMMIT;
        select 0 as error;
    END IF;

END//
delimiter ;



SET NAMES utf8;
DROP PROCEDURE IF EXISTS `sp_update_comment_to_shift_attendance_by_person_id`;
delimiter //
CREATE PROCEDURE `sp_update_comment_to_shift_attendance_by_person_id`(
	IN `pr_person_id` INT,
	IN `pr_from_date` DATE,
	IN `pr_to_date` DATE,
	IN `pr_new_comment` TEXT,
	IN `pr_old_comment` TEXT,
	IN `pr_account_id` INT,
	OUT `return_success_status` TINYINT

)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'when submit leaving application then update column comment in table shift_attendance'
update_shift_attendance_comment:BEGIN

	UPDATE shift_attendance SA INNER JOIN class_shift_work CSW ON SA.class_shift_work_id = CSW.id
	SET SA.comment = pr_new_comment,
		SA.lastup_account_id = pr_account_id,
		SA.lastup_datetime = now()
	WHERE
		person_id = pr_person_id
		and SA.disable = 0
		and CSW.disable = 0
		and SA.comment = pr_old_comment
		and CSW.date in (SELECT date FROM school_calendar WHERE disable = 0 AND is_school_holiday = 0 AND is_public_holiday = 0 AND date >= pr_from_date AND date <= pr_to_date)
	;

	SET return_success_status = 1;
END//
delimiter ;



SET NAMES utf8;
DROP PROCEDURE IF EXISTS `sp_update_leave_absence_application`;
delimiter //
CREATE PROCEDURE `sp_update_leave_absence_application`(
	IN `pr_application_id` INT
    ,
	IN `pr_application_date` DATE
    ,
	IN `pr_leaving_date` DATE
    ,
	IN `pr_leaving_return_country_date` DATE
    ,
	IN `pr_leaving_return_country_phone_no` VARCHAR(20)
    ,
	IN `pr_leaving_return_country_mail_address` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address1` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address2` VARCHAR(255)
    ,
	IN `pr_leaving_return_country_address3` VARCHAR(255),
	IN `pr_leaving_reason` TEXT
    ,
	IN `pr_leaving_reason_detail` TEXT
    ,
	IN `pr_leaving_note` TEXT
,
	IN `pr_lastup_account_id` INT


)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT 'Update leaving application info'
update_app:BEGIN
	DECLARE `_rollback` BOOL DEFAULT 0;
	DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;

	/* get application type by column name */
	 SET @app_type_id = fn_get_id_by_column_name('application_type_id', 'leave_absence');

	SET @status_cd = (SELECT status_cd FROM application WHERE id = pr_application_id and application_type_id = @app_type_id and disable = 0);
    /* check exists */
    IF @status_cd IS NULL THEN
        SELECT 1 as error, 'application not found' as errmsg;
        LEAVE update_app;
    END IF;

	 START TRANSACTION;
    UPDATE application
    SET
    	  status_cd = 1,
        application_date = pr_application_date,
        lastup_account_id = pr_lastup_account_id,
        lastup_datetime = now()
    WHERE
        id = pr_application_id;


	/* Update application properties */
	UPDATE application_leave_absence_properties
	SET
		lastup_account_id = pr_lastup_account_id,
		lastup_datetime = now(),
		leave_absence_date_from = pr_leaving_date,
       leave_absence_date_to = pr_leaving_return_country_date,
       leave_absence_return_country_phone_no = pr_leaving_return_country_phone_no,
       leave_absence_return_country_mail_address = pr_leaving_return_country_mail_address,
       leave_absence_return_country_address1 = pr_leaving_return_country_address1,
       leave_absence_return_country_address2 = pr_leaving_return_country_address2,
       leave_absence_return_country_address3 = pr_leaving_return_country_address3,
       leave_absence_reason_id = pr_leaving_reason,
       leave_absence_reason_detail = pr_leaving_reason_detail,
       leave_absence_note = pr_leaving_note
	WHERE application_id = pr_application_id;

	IF `_rollback` THEN
        ROLLBACK;
        select 1 as error;
    ELSE
        COMMIT;
        select 0 as error;
    END IF;
END//
delimiter ;



