-- MySQL dump 10.13  Distrib 5.6.33, for Linux (x86_64)
--
-- Host: localhost    Database: midream_stg
-- ------------------------------------------------------
-- Server version	5.6.33-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ability`
--

DROP TABLE IF EXISTS `ability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ability` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ability_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `external_test_level_id` int(10) unsigned NOT NULL DEFAULT '0',
  `next_ability_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `color` char(6) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `main_text` text NOT NULL,
  `text1` text NOT NULL,
  `text2` text NOT NULL,
  `text3` text NOT NULL,
  `text4` text NOT NULL,
  `text5` text NOT NULL,
  `text6` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `test_level_id` (`external_test_level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ability_group`
--

DROP TABLE IF EXISTS `ability_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ability_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `login_id` varchar(64) NOT NULL DEFAULT '',
  `password` varchar(64) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `login_attempt_fail` int(10) unsigned NOT NULL DEFAULT '0',
  `locked_expire_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_group_id` (`account_group_id`),
  KEY `person_id` (`person_id`),
  KEY `login_id` (`login_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2020 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `account_group`
--

DROP TABLE IF EXISTS `account_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `account_title`
--

DROP TABLE IF EXISTS `account_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_title` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(225) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `is_school_fee` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `amount` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `achievement`
--

DROP TABLE IF EXISTS `achievement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `achievement` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `content_test_list_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_date` date NOT NULL DEFAULT '0000-00-00',
  `total_score` int(10) unsigned NOT NULL DEFAULT '0',
  `is_additional_test` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `content_test_list_id` (`content_test_list_id`),
  KEY `class_id` (`class_id`),
  KEY `test_date` (`test_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `achievement_detail`
--

DROP TABLE IF EXISTS `achievement_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `achievement_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `achievement_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_section_cd` int(3) unsigned NOT NULL DEFAULT '0',
  `score` int(10) unsigned NOT NULL DEFAULT '0',
  `score_original` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `achievement_id` (`achievement_id`),
  KEY `test_section_cd` (`test_section_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `agent`
--

DROP TABLE IF EXISTS `agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agent` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `individual_corporate_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `representative_last_name` varchar(128) NOT NULL DEFAULT '',
  `representative_last_name_kana` varchar(128) NOT NULL DEFAULT '',
  `representative_first_name` varchar(128) NOT NULL DEFAULT '',
  `representative_first_name_kana` varchar(128) NOT NULL DEFAULT '',
  `representative_first_name_alphabet` varchar(128) NOT NULL DEFAULT '',
  `representative_last_name_alphabet` varchar(128) NOT NULL DEFAULT '',
  `in_charge_division` varchar(128) NOT NULL DEFAULT '',
  `in_charge_last_name` varchar(128) NOT NULL DEFAULT '',
  `in_charge_first_name` varchar(128) NOT NULL DEFAULT '',
  `in_charge_last_name_kana` varchar(128) NOT NULL DEFAULT '',
  `in_charge_first_name_kana` varchar(128) NOT NULL DEFAULT '',
  `in_charge_last_name_alphabet` varchar(128) DEFAULT '',
  `in_charge_first_name_alphabet` varchar(128) DEFAULT '',
  `in_charge_phone_no1` varchar(20) NOT NULL DEFAULT '',
  `in_charge_phone_no2` varchar(20) NOT NULL DEFAULT '',
  `in_charge_mail_address` varchar(255) NOT NULL DEFAULT '',
  `in_charge_qq_no` varchar(20) NOT NULL DEFAULT '',
  `school_in_charge` int(10) NOT NULL DEFAULT '0',
  `school_in_charge_comment` text,
  `commission_fee` float DEFAULT NULL,
  `commission_fee_comment` text,
  `document_charge_last_name` varchar(128) NOT NULL DEFAULT '',
  `document_charge_first_name` varchar(128) NOT NULL DEFAULT '',
  `document_charge_phone_no1` varchar(20) NOT NULL DEFAULT '',
  `document_charge_phone_no2` varchar(20) NOT NULL DEFAULT '',
  `document_charge_qq_no` varchar(12) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `application`
--

DROP TABLE IF EXISTS `application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_id` int(10) unsigned NOT NULL DEFAULT '0',
  `application_no` varchar(10) NOT NULL DEFAULT '',
  `application_type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `application_date` date NOT NULL DEFAULT '0000-00-00',
  `status_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `charge_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_paid` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `print_date` date NOT NULL DEFAULT '0000-00-00',
  `authorized_absent_date_from` date NOT NULL DEFAULT '0000-00-00',
  `authorized_absent_date_to` date NOT NULL DEFAULT '0000-00-00',
  `confirmed_absent_date_from` date NOT NULL DEFAULT '0000-00-00',
  `confirmed_absent_date_to` date NOT NULL DEFAULT '0000-00-00',
  `authorized_absent_reason_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `authorized_absent_school_name` varchar(128) NOT NULL DEFAULT '',
  `authorized_absent_school_section` varchar(128) NOT NULL DEFAULT '',
  `authorized_absent_professor` varchar(128) NOT NULL DEFAULT '',
  `authorized_absent_ceremony_relation` varchar(128) NOT NULL DEFAULT '',
  `authorized_absent_company` varchar(128) NOT NULL DEFAULT '',
  `authorized_absent_comment` text NOT NULL,
  `temporary_return_date_from` date NOT NULL DEFAULT '0000-00-00',
  `temporary_return_date_to` date NOT NULL DEFAULT '0000-00-00',
  `temporary_return_reason` text NOT NULL,
  `temporary_return_contact_name` varchar(128) NOT NULL DEFAULT '',
  `temporary_return_contact_phone_no` varchar(20) NOT NULL DEFAULT '',
  `temporary_return_address` varchar(255) NOT NULL DEFAULT '',
  `is_temporary_return_main_teacher_confirm` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_temporary_return_office_staff_confirm` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `temporary_return_comment` text NOT NULL,
  `personal_info_last_name` varchar(128) NOT NULL DEFAULT '',
  `personal_info_first_name` varchar(128) NOT NULL DEFAULT '',
  `personal_info_last_name_kana` varchar(128) NOT NULL DEFAULT '',
  `personal_info_first_name_kana` varchar(128) NOT NULL DEFAULT '',
  `personal_info_last_name_alphabet` varchar(128) NOT NULL DEFAULT '',
  `personal_info_first_name_alphabet` varchar(128) NOT NULL DEFAULT '',
  `personal_info_zipcode` varchar(12) NOT NULL DEFAULT '',
  `personal_info_address1` varchar(255) NOT NULL DEFAULT '',
  `personal_info_address2` varchar(255) NOT NULL DEFAULT '',
  `personal_info_phone_no` varchar(20) NOT NULL DEFAULT '',
  `part_time_job_company_name` varchar(128) NOT NULL DEFAULT '',
  `part_time_job_category` varchar(128) NOT NULL DEFAULT '',
  `part_time_job_zipcode` varchar(12) NOT NULL DEFAULT '',
  `part_time_job_address1` varchar(255) NOT NULL DEFAULT '',
  `part_time_job_address2` varchar(255) NOT NULL DEFAULT '',
  `part_time_job_phone_no` varchar(20) NOT NULL DEFAULT '',
  `part_time_job_phone_no_title` varchar(128) NOT NULL DEFAULT '',
  `part_time_job_monday_time_from` time NOT NULL DEFAULT '00:00:00',
  `part_time_job_monday_time_to` time NOT NULL DEFAULT '00:00:00',
  `part_time_job_monday_break` time NOT NULL DEFAULT '00:00:00',
  `part_time_job_tuesday_time_from` time NOT NULL DEFAULT '00:00:00',
  `part_time_job_tuesday_time_to` time NOT NULL DEFAULT '00:00:00',
  `part_time_job_tuesday_break` time NOT NULL DEFAULT '00:00:00',
  `part_time_job_wednesday_time_from` time NOT NULL DEFAULT '00:00:00',
  `part_time_job_wednesday_time_to` time NOT NULL DEFAULT '00:00:00',
  `part_time_job_wednesday_break` time NOT NULL DEFAULT '00:00:00',
  `part_time_job_thursday_time_from` time NOT NULL DEFAULT '00:00:00',
  `part_time_job_thursday_time_to` time NOT NULL DEFAULT '00:00:00',
  `part_time_job_thursday_break` time NOT NULL DEFAULT '00:00:00',
  `part_time_job_friday_time_from` time NOT NULL DEFAULT '00:00:00',
  `part_time_job_friday_time_to` time NOT NULL DEFAULT '00:00:00',
  `part_time_job_friday_break` time NOT NULL DEFAULT '00:00:00',
  `part_time_job_saturday_time_from` time NOT NULL DEFAULT '00:00:00',
  `part_time_job_saturday_time_to` time NOT NULL DEFAULT '00:00:00',
  `part_time_job_saturday_break` time NOT NULL DEFAULT '00:00:00',
  `part_time_job_sunday_time_from` time NOT NULL DEFAULT '00:00:00',
  `part_time_job_sunday_time_to` time NOT NULL DEFAULT '00:00:00',
  `part_time_job_sunday_break` time NOT NULL DEFAULT '00:00:00',
  `class_change_month` date NOT NULL DEFAULT '0000-00-00',
  `is_class_change_teacher_confirm` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `class_change_time_zone` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `class_change_reason` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `class_change_reason_comment` text NOT NULL,
  `class_change_comment` text NOT NULL,
  `certificate_purpose_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `certificate_school_route_from` varchar(128) NOT NULL DEFAULT '',
  `certificate_school_route_to` varchar(128) NOT NULL DEFAULT '',
  `certificate_school_route_line` varchar(255) NOT NULL DEFAULT '',
  `certificate_close_school_date_from` date NOT NULL DEFAULT '0000-00-00',
  `certificate_close_school_date_to` date NOT NULL DEFAULT '0000-00-00',
  `certificate_not_recommend_reason` text NOT NULL,
  `certificate_college_name` varchar(128) NOT NULL DEFAULT '',
  `certificate_college_section` varchar(128) NOT NULL DEFAULT '',
  `certificate_postgraduate_name` varchar(128) NOT NULL DEFAULT '',
  `certificate_postgraduate_section` varchar(128) NOT NULL DEFAULT '',
  `certificate_career_college_name` varchar(128) NOT NULL DEFAULT '',
  `certificate_career_college_section` varchar(128) NOT NULL DEFAULT '',
  `certificate_destination` varchar(255) NOT NULL DEFAULT '',
  `certificate_other` varchar(128) NOT NULL DEFAULT '',
  `leaving_date` date NOT NULL DEFAULT '0000-00-00',
  `leaving_return_country_date` date NOT NULL DEFAULT '0000-00-00',
  `leaving_return_country_phone_no` varchar(20) NOT NULL DEFAULT '',
  `leaving_return_country_mail_address` varchar(255) NOT NULL DEFAULT '',
  `leaving_return_country_address1` varchar(255) NOT NULL DEFAULT '',
  `leaving_return_country_address2` varchar(255) NOT NULL DEFAULT '',
  `leaving_reason` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `request_id` (`request_id`),
  KEY `application_type_id` (`application_type_id`),
  KEY `charge_id` (`charge_id`),
  KEY `application_no` (`application_no`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `application_approve`
--

DROP TABLE IF EXISTS `application_approve`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_approve` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `application_id` int(10) unsigned NOT NULL DEFAULT '0',
  `application_approver_id` int(10) unsigned NOT NULL DEFAULT '0',
  `approver_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `status_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `action` text NOT NULL,
  `interview_id` int(10) unsigned NOT NULL DEFAULT '0',
  `approval_date` date NOT NULL DEFAULT '0000-00-00',
  `confirmed_document` varchar(255) NOT NULL DEFAULT '',
  `reject_reason` varchar(255) NOT NULL DEFAULT '',
  `comment` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `application_id` (`application_id`),
  KEY `application_approver_id` (`application_approver_id`),
  KEY `interview_id` (`interview_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `application_approver`
--

DROP TABLE IF EXISTS `application_approver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_approver` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL DEFAULT '',
  `approver_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `application_type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `application_type_id` (`application_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `application_type`
--

DROP TABLE IF EXISTS `application_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(225) NOT NULL DEFAULT '',
  `is_certification` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `prefix_character` varchar(3) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `attendance`
--

DROP TABLE IF EXISTS `attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendance` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_shift_work_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_work_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attendance` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `time_from` time NOT NULL DEFAULT '00:00:00',
  `time_to` time NOT NULL DEFAULT '00:00:00',
  `is_authorized_absent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_leave_absent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_canceled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_temporary_return` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `class_work_id` (`class_work_id`),
  KEY `person_id` (`person_id`),
  KEY `class_shift_work_id` (`class_shift_work_id`)
) ENGINE=InnoDB AUTO_INCREMENT=229833 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `attendance_edit_history`
--

DROP TABLE IF EXISTS `attendance_edit_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendance_edit_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `attendance_id` int(10) unsigned NOT NULL DEFAULT '0',
  `status_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `original_staff_person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `original_attendance` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `edit_staff_person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_attendance` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `original_time_from` time NOT NULL DEFAULT '00:00:00',
  `edit_time_from` time NOT NULL DEFAULT '00:00:00',
  `original_time_to` time NOT NULL DEFAULT '00:00:00',
  `edit_time_to` time NOT NULL DEFAULT '00:00:00',
  `edit_reason` varchar(255) NOT NULL DEFAULT '',
  `reject_reason` varchar(255) NOT NULL DEFAULT '',
  `is_confirmed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `attendance_id` (`attendance_id`),
  KEY `original_staff_person_id` (`original_staff_person_id`),
  KEY `edit_staff_person_id` (`edit_staff_person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `attendance_rate`
--

DROP TABLE IF EXISTS `attendance_rate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendance_rate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(10) unsigned NOT NULL DEFAULT '0',
  `rate_time` int(7) unsigned NOT NULL DEFAULT '0',
  `total_time` int(10) unsigned NOT NULL DEFAULT '0',
  `attend_time` int(10) unsigned NOT NULL DEFAULT '0',
  `absent_time` int(10) unsigned NOT NULL DEFAULT '0',
  `late_time` int(10) unsigned NOT NULL DEFAULT '0',
  `rate_day` int(7) unsigned NOT NULL DEFAULT '0',
  `total_day` int(10) unsigned NOT NULL DEFAULT '0',
  `attend_day` int(10) unsigned NOT NULL DEFAULT '0',
  `absent_day` int(10) unsigned NOT NULL DEFAULT '0',
  `attendance_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `calculate_date_from` date NOT NULL DEFAULT '0000-00-00',
  `calculate_date_to` date NOT NULL DEFAULT '0000-00-00',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55691 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `attendance_rate_rank`
--

DROP TABLE IF EXISTS `attendance_rate_rank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendance_rate_rank` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `color` char(6) NOT NULL DEFAULT '',
  `threshold_rate` int(7) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `charge`
--

DROP TABLE IF EXISTS `charge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `charge` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(10) unsigned NOT NULL DEFAULT '0',
  `account_title_id` int(10) unsigned NOT NULL DEFAULT '0',
  `amount` int(10) unsigned NOT NULL DEFAULT '0',
  `due_date` date NOT NULL DEFAULT '0000-00-00',
  `pay_off_date` date NOT NULL DEFAULT '0000-00-00',
  `comment` text NOT NULL,
  `status` tinyint(2) unsigned NOT NULL DEFAULT '0',
  `confirm_date` date NOT NULL DEFAULT '0000-00-00',
  `paid_person_name` varchar(128) NOT NULL DEFAULT '',
  `confirm_person_name` varchar(128) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  KEY `account_title_id` (`account_title_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class`
--

DROP TABLE IF EXISTS `class`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `teacher_person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `time_zone_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_room` varchar(128) NOT NULL DEFAULT '',
  `text_book` varchar(255) NOT NULL DEFAULT '',
  `time_table_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `teacher_person_id` (`teacher_person_id`),
  KEY `class_group_id` (`class_group_id`),
  KEY `time_zone_id` (`time_zone_id`),
  KEY `time_table_group_id` (`time_table_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=227 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_entry_exit`
--

DROP TABLE IF EXISTS `class_entry_exit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_entry_exit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_shift_work_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_time` time NOT NULL DEFAULT '00:00:00',
  `exit_time` time NOT NULL DEFAULT '00:00:00',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `class_shift_work_id` (`class_shift_work_id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29092 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_group`
--

DROP TABLE IF EXISTS `class_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_group_unit_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ability_id` int(10) unsigned NOT NULL DEFAULT '0',
  `main_teacher_person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date_from` date NOT NULL DEFAULT '0000-00-00',
  `date_to` date NOT NULL DEFAULT '0000-00-00',
  `is_special` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ability_id` (`ability_id`),
  KEY `main_teacher_person_id` (`main_teacher_person_id`),
  KEY `school_term_id` (`school_term_id`),
  KEY `date_from` (`date_from`),
  KEY `date_to` (`date_to`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_group_template`
--

DROP TABLE IF EXISTS `class_group_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_group_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ability_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ability_id` (`ability_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_group_unit`
--

DROP TABLE IF EXISTS `class_group_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_group_unit` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date_from` date NOT NULL DEFAULT '0000-00-00',
  `date_to` date NOT NULL DEFAULT '0000-00-00',
  `is_confirmed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `comment1` text NOT NULL,
  `comment2` text NOT NULL,
  `comment3` text NOT NULL,
  `comment4` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_shift`
--

DROP TABLE IF EXISTS `class_shift`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_shift` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_id` int(10) unsigned NOT NULL DEFAULT '0',
  `day` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `teacher_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_optional` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `class_id` (`class_id`),
  KEY `teacher_id` (`teacher_id`)
) ENGINE=InnoDB AUTO_INCREMENT=999 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_shift_request_level`
--

DROP TABLE IF EXISTS `class_shift_request_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_shift_request_level` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `teacher_school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `external_test_level_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `teacher_school_term_id` (`teacher_school_term_id`),
  KEY `test_level_id` (`external_test_level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_shift_request_time_zone`
--

DROP TABLE IF EXISTS `class_shift_request_time_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_shift_request_time_zone` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `teacher_school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `time_zone_id` int(10) unsigned NOT NULL DEFAULT '0',
  `day` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `possible_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `teacher_school_term_id` (`teacher_school_term_id`),
  KEY `time_zone_id` (`time_zone_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_shift_work`
--

DROP TABLE IF EXISTS `class_shift_work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_shift_work` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_shift_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `class_shift_id` (`class_shift_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2841 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_student`
--

DROP TABLE IF EXISTS `class_student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_student` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date_from` date NOT NULL DEFAULT '0000-00-00',
  `date_to` date NOT NULL DEFAULT '0000-00-00',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `class_id` (`class_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3585 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_template`
--

DROP TABLE IF EXISTS `class_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `class_group_template_id` int(10) unsigned NOT NULL DEFAULT '0',
  `time_zone_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_room` varchar(128) NOT NULL DEFAULT '',
  `text_book` varchar(255) NOT NULL DEFAULT '',
  `time_table_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `class_group_template_id` (`class_group_template_id`),
  KEY `time_zone_id` (`time_zone_id`),
  KEY `time_table_group_id` (`time_table_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `class_work`
--

DROP TABLE IF EXISTS `class_work`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `class_work` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `period_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_shift_work_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_canceled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `period_id` (`period_id`),
  KEY `class_shift_work_id` (`class_shift_work_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11362 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_level`
--

DROP TABLE IF EXISTS `content_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_level` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_level_ability`
--

DROP TABLE IF EXISTS `content_level_ability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_level_ability` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_level_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ability_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_target` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_level_id` (`content_level_id`),
  KEY `ability_id` (`ability_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_test_answer`
--

DROP TABLE IF EXISTS `content_test_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_test_answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_question_id` int(10) unsigned NOT NULL DEFAULT '0',
  `answer_sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `answer` text NOT NULL,
  `is_correct` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_test_question_id` (`content_test_question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_test_answer_selection`
--

DROP TABLE IF EXISTS `content_test_answer_selection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_test_answer_selection` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_question_id` int(10) unsigned NOT NULL DEFAULT '0',
  `selection_sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `hint` text NOT NULL,
  `answer_selection` varchar(50) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_test_question_id` (`content_test_question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_test_list`
--

DROP TABLE IF EXISTS `content_test_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_test_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `test_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ability_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(256) NOT NULL DEFAULT '',
  `total_point` int(10) unsigned NOT NULL DEFAULT '0',
  `threshold_rate` int(7) unsigned NOT NULL DEFAULT '0',
  `time_test` int(10) unsigned NOT NULL DEFAULT '0',
  `public_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ability_id` (`ability_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_test_question`
--

DROP TABLE IF EXISTS `content_test_question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_test_question` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_list_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_section_cd` int(3) unsigned NOT NULL DEFAULT '0',
  `test_title` text NOT NULL,
  `test_kind` varchar(50) NOT NULL DEFAULT '',
  `question_sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `score` int(10) unsigned NOT NULL DEFAULT '0',
  `score_kind` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `question` text NOT NULL,
  `hint` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `test_list_id` (`content_test_list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_test_section`
--

DROP TABLE IF EXISTS `content_test_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_test_section` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_list_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_section_cd` int(3) unsigned NOT NULL DEFAULT '0',
  `total_point` int(10) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_test_list_id` (`content_test_list_id`),
  KEY `test_section_cd` (`test_section_cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `content_test_status`
--

DROP TABLE IF EXISTS `content_test_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `content_test_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_list_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `total_score` int(10) unsigned NOT NULL DEFAULT '0',
  `status` int(3) unsigned NOT NULL DEFAULT '0',
  `is_teacher` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_test_list_id` (`content_test_list_id`),
  KEY `class_id` (`class_id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `country_cd` int(3) unsigned zerofill NOT NULL DEFAULT '000',
  `name` varchar(128) NOT NULL DEFAULT '',
  `english_name` varchar(128) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `country_cd` (`country_cd`)
) ENGINE=InnoDB AUTO_INCREMENT=257 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `course` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `english_name` varchar(255) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `duration`
--

DROP TABLE IF EXISTS `duration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `duration` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from` date NOT NULL DEFAULT '0000-00-00',
  `to` date NOT NULL DEFAULT '0000-00-00',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `event_type`
--

DROP TABLE IF EXISTS `event_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL DEFAULT '',
  `color` char(6) NOT NULL DEFAULT '',
  `school_year_id_from` int(10) unsigned NOT NULL DEFAULT '0',
  `school_year_id_to` int(10) unsigned NOT NULL DEFAULT '0',
  `is_on_holiday` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `school_year_id_from` (`school_year_id_from`),
  KEY `school_year_id_to` (`school_year_id_to`),
  KEY `function_group_id` (`is_on_holiday`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `event_type_account_group`
--

DROP TABLE IF EXISTS `event_type_account_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_type_account_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `account_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `event_type_id` (`event_type_id`),
  KEY `account_group_id` (`account_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=140 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `examination_answer`
--

DROP TABLE IF EXISTS `examination_answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `examination_answer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `content_test_question_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `achievement_id` int(10) unsigned NOT NULL DEFAULT '0',
  `test_section_cd` int(3) unsigned NOT NULL DEFAULT '0',
  `answer_position` int(10) unsigned NOT NULL DEFAULT '0',
  `answer_comment` text NOT NULL,
  `content_test_answer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `score` varchar(10) NOT NULL DEFAULT '',
  `test_kind` varchar(50) NOT NULL DEFAULT '',
  `is_correct` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_test_question_id` (`content_test_question_id`),
  KEY `person_id` (`person_id`),
  KEY `achievement_id` (`achievement_id`),
  KEY `selected_content_test_answer_id` (`content_test_answer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `external_test`
--

DROP TABLE IF EXISTS `external_test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `column_name` varchar(128) NOT NULL DEFAULT '',
  `is_for_student` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_for_staff` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `column_name` (`column_name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `external_test_level`
--

DROP TABLE IF EXISTS `external_test_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `external_test_level` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `external_test_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `external_test_id` (`external_test_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `function`
--

DROP TABLE IF EXISTS `function`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `function` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `function_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `path` varchar(128) NOT NULL DEFAULT '',
  `is_in_menu` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `function_group_id` (`function_group_id`),
  KEY `is_in_menu` (`is_in_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `function_group`
--

DROP TABLE IF EXISTS `function_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `function_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `holiday_setting`
--

DROP TABLE IF EXISTS `holiday_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `holiday_setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `color` char(6) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `immigration`
--

DROP TABLE IF EXISTS `immigration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `immigration` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `immigration_application_no` varchar(45) NOT NULL DEFAULT '',
  `examination_no` varchar(10) NOT NULL DEFAULT '',
  `visa_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `entry_date` date NOT NULL DEFAULT '0000-00-00',
  `visa_expired_date` date NOT NULL DEFAULT '0000-00-00',
  `passport_no` varchar(128) NOT NULL DEFAULT '',
  `passport_expired_date` date NOT NULL DEFAULT '0000-00-00',
  `residence_card_no` varchar(128) NOT NULL DEFAULT '',
  `home_country_id` int(10) unsigned NOT NULL DEFAULT '0',
  `home_country_zipcode` varchar(12) NOT NULL DEFAULT '',
  `home_country_address1` varchar(255) NOT NULL DEFAULT '',
  `home_country_address2` varchar(255) NOT NULL DEFAULT '',
  `home_country_address3` varchar(255) NOT NULL DEFAULT '',
  `home_country_address_font` varchar(225) NOT NULL DEFAULT '',
  `home_country_phone_no1` varchar(20) NOT NULL DEFAULT '',
  `home_country_phone_no2` varchar(20) NOT NULL DEFAULT '',
  `ability_proof_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ability_proof_test_name` varchar(255) NOT NULL DEFAULT '',
  `ability_proof_level_score` varchar(255) NOT NULL DEFAULT '',
  `ability_proof_institution_name` varchar(255) NOT NULL DEFAULT '',
  `ability_proof_enrollment_start` date NOT NULL DEFAULT '0000-00-00',
  `ability_proof_enrollment_end` date NOT NULL DEFAULT '0000-00-00',
  `ability_proof_other_comment` text NOT NULL,
  `agent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `intended_visa_apply_place` varchar(255) NOT NULL DEFAULT '',
  `is_past_entry_departure` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `past_entry_times` int(2) unsigned NOT NULL DEFAULT '0',
  `last_entry_date` date NOT NULL DEFAULT '0000-00-00',
  `last_departure_date` date NOT NULL DEFAULT '0000-00-00',
  `last_school_education_status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `last_school_education` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `last_school_education_name` varchar(255) NOT NULL DEFAULT '',
  `last_school_education_graduation_date` varchar(45) NOT NULL DEFAULT '',
  `support_pay_amount_self` int(10) unsigned NOT NULL DEFAULT '0',
  `support_pay_comment` varchar(225) NOT NULL DEFAULT '',
  `remittance_from_abroad_amount` int(10) unsigned NOT NULL DEFAULT '0',
  `carrying_from_abroad_amount` int(10) unsigned NOT NULL DEFAULT '0',
  `support_pay_amount_living_japan` int(10) unsigned NOT NULL DEFAULT '0',
  `support_pay_amount_others` int(10) unsigned NOT NULL DEFAULT '0',
  `supporter_last_name` varchar(128) NOT NULL DEFAULT '',
  `supporter_first_name` varchar(128) NOT NULL DEFAULT '',
  `supporter_relationship` varchar(12) NOT NULL DEFAULT '',
  `supporter_zipcode` varchar(12) NOT NULL,
  `supporter_address1` varchar(255) NOT NULL DEFAULT '',
  `supporter_address2` varchar(255) NOT NULL DEFAULT '',
  `supporter_address3` varchar(255) NOT NULL DEFAULT '',
  `supporter_phone_no1` varchar(20) NOT NULL DEFAULT '',
  `supporter_phone_no2` varchar(20) NOT NULL DEFAULT '',
  `supporter_job_type` varchar(128) NOT NULL DEFAULT '',
  `supporter_employer_name` varchar(128) NOT NULL DEFAULT '',
  `supporter_employer_address1` varchar(128) NOT NULL DEFAULT '',
  `supporter_employer_address2` varchar(128) NOT NULL DEFAULT '',
  `supporter_employer_address3` varchar(128) NOT NULL DEFAULT '',
  `supporter_employer_phone_no1` varchar(20) NOT NULL DEFAULT '',
  `supporter_employer_phone_no2` varchar(20) NOT NULL DEFAULT '',
  `supporter_employer_type` varchar(128) NOT NULL DEFAULT '',
  `supporter_employed_position` varchar(128) NOT NULL DEFAULT '',
  `supporter_employed_years` int(2) unsigned NOT NULL DEFAULT '0',
  `supporter_annual_income_amount` int(10) unsigned NOT NULL DEFAULT '0',
  `supporter_annual_income_currency` varchar(128) NOT NULL DEFAULT '',
  `total_education_years` int(2) unsigned NOT NULL DEFAULT '0',
  `birth_address` varchar(255) NOT NULL DEFAULT '',
  `eju_score` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`home_country_id`),
  KEY `person_id` (`person_id`),
  KEY `home_country_id` (`home_country_id`),
  KEY `agent_id` (`agent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `interview`
--

DROP TABLE IF EXISTS `interview`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interview` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `comment` text NOT NULL,
  `interviewer_person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `interviewer_person_id` (`interviewer_person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `late_threshold`
--

DROP TABLE IF EXISTS `late_threshold`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `late_threshold` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `late_attend` int(3) unsigned NOT NULL DEFAULT '0',
  `absent_late` int(3) unsigned NOT NULL DEFAULT '0',
  `late_as_absent` int(3) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uri` varchar(255) NOT NULL DEFAULT '',
  `method` varchar(255) NOT NULL DEFAULT '',
  `params` text,
  `ip_address` varchar(45) NOT NULL DEFAULT '',
  `time` int(11) NOT NULL DEFAULT '0',
  `rtime` float DEFAULT '0',
  `authorized` varchar(1) NOT NULL DEFAULT '0',
  `response_code` smallint(3) DEFAULT '0',
  `response` text,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4816 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `maxrow`
--

DROP TABLE IF EXISTS `maxrow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `maxrow` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `maxrow` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `monthly_status`
--

DROP TABLE IF EXISTS `monthly_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `monthly_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `month` date NOT NULL DEFAULT '0000-00-00',
  `is_attendance_confirmed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `movie`
--

DROP TABLE IF EXISTS `movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `link` varchar(512) NOT NULL DEFAULT '',
  `is_publish` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `content_level_id` int(10) NOT NULL DEFAULT '0',
  `is_examination` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `examination_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `examination_id` (`examination_id`),
  KEY `content_level_id` (`content_level_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `movie_status`
--

DROP TABLE IF EXISTS `movie_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movie_status` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `movie_id` int(10) unsigned NOT NULL DEFAULT '0',
  `account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `status` int(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `movie_id` (`movie_id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `operation_history`
--

DROP TABLE IF EXISTS `operation_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operation_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `logout_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `mac_address` varchar(20) NOT NULL DEFAULT '',
  `ip_address` varchar(15) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1814 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `period`
--

DROP TABLE IF EXISTS `period`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `period` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `duration_id` int(10) unsigned NOT NULL DEFAULT '0',
  `time_zone_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `is_extra` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `duration_id` (`duration_id`),
  KEY `time_zone_id` (`time_zone_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `function_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_permitted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_group_id` (`account_group_id`),
  KEY `function_group_id` (`function_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(3) unsigned NOT NULL DEFAULT '0',
  `country_id` int(10) unsigned NOT NULL DEFAULT '0',
  `native_language` varchar(128) NOT NULL DEFAULT '',
  `parent_person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `birthday` date NOT NULL DEFAULT '0000-00-00',
  `sex_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `married_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `zipcode` varchar(12) NOT NULL DEFAULT '',
  `address_country_id` int(10) unsigned NOT NULL DEFAULT '0',
  `address1` varchar(255) NOT NULL DEFAULT '',
  `address2` varchar(255) NOT NULL DEFAULT '',
  `address3` varchar(255) NOT NULL DEFAULT '',
  `phone_no` varchar(20) NOT NULL DEFAULT '',
  `phone_no2` varchar(20) NOT NULL DEFAULT '',
  `mobile_no` varchar(20) NOT NULL DEFAULT '',
  `previous_address_zipcode` varchar(12) NOT NULL DEFAULT '',
  `previous_address1` varchar(255) NOT NULL DEFAULT '',
  `previous_address2` varchar(255) NOT NULL DEFAULT '',
  `previous_address3` varchar(255) NOT NULL DEFAULT '',
  `mail_address1` varchar(255) NOT NULL DEFAULT '',
  `mail_address2` varchar(255) NOT NULL DEFAULT '',
  `last_name` varchar(128) NOT NULL DEFAULT '',
  `first_name` varchar(128) NOT NULL DEFAULT '',
  `name_font` varchar(255) NOT NULL DEFAULT '',
  `last_name_kana` varchar(128) NOT NULL DEFAULT '',
  `first_name_kana` varchar(128) NOT NULL DEFAULT '',
  `last_name_alphabet` varchar(128) NOT NULL DEFAULT '',
  `first_name_alphabet` varchar(128) NOT NULL DEFAULT '',
  `legal_registered_last_name` varchar(128) NOT NULL DEFAULT '',
  `legal_registered_first_name` varchar(128) NOT NULL DEFAULT '',
  `legal_registered_last_name_kana` varchar(128) NOT NULL DEFAULT '',
  `legal_registered_first_name_kana` varchar(128) NOT NULL DEFAULT '',
  `legal_registered_last_name_alphabet` varchar(128) NOT NULL DEFAULT '',
  `legal_registered_first_name_alphabet` varchar(128) NOT NULL DEFAULT '',
  `abbreviation_name` varchar(128) NOT NULL DEFAULT '',
  `photo` varchar(128) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`),
  KEY `parent_person_id` (`parent_person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2030 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quick_menu`
--

DROP TABLE IF EXISTS `quick_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quick_menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `account_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_group_id` (`account_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `quick_menu_detail`
--

DROP TABLE IF EXISTS `quick_menu_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quick_menu_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quick_menu_id` int(10) unsigned NOT NULL DEFAULT '0',
  `function_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `quick_menu_id` (`quick_menu_id`),
  KEY `function_id` (`function_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `request`
--

DROP TABLE IF EXISTS `request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `request` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `request_no` varchar(128) NOT NULL DEFAULT '',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school`
--

DROP TABLE IF EXISTS `school`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `english_name` varchar(128) NOT NULL DEFAULT '',
  `principal_name` varchar(128) NOT NULL DEFAULT '',
  `english_principal_name` varchar(128) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `country_id` int(10) unsigned NOT NULL DEFAULT '0',
  `zipcode` varchar(12) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `english_address` varchar(128) NOT NULL DEFAULT '',
  `phone_no` varchar(16) NOT NULL DEFAULT '',
  `fax_no` varchar(16) NOT NULL DEFAULT '',
  `international_phone_no` varchar(20) NOT NULL DEFAULT '',
  `international_fax_no` varchar(20) NOT NULL DEFAULT '',
  `school_type_certification_no` varchar(4) NOT NULL DEFAULT '',
  `immigration_office` varchar(255) NOT NULL DEFAULT '',
  `immigration_office_department` varchar(255) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `country_id` (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_calendar`
--

DROP TABLE IF EXISTS `school_calendar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_calendar` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `school_year_id` int(10) unsigned NOT NULL DEFAULT '0',
  `school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `is_public_holiday` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_school_holiday` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `school_year_id` (`school_year_id`)
) ENGINE=InnoDB AUTO_INCREMENT=731 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_event`
--

DROP TABLE IF EXISTS `school_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `school_year_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `event_type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `remark` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `school_year_id` (`school_year_id`),
  KEY `event_type_id` (`event_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_record`
--

DROP TABLE IF EXISTS `school_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class_student_id` int(10) unsigned NOT NULL DEFAULT '0',
  `observation_internal` text NOT NULL,
  `observation_internal_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `observation_external` text NOT NULL,
  `observation_external_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `observation_external_english` text NOT NULL,
  `observation_external_english_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `homework_submission_rate` int(7) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `class_student_id` (`class_student_id`),
  KEY `observation_internal_account_id` (`observation_internal_account_id`),
  KEY `observation_external_account_id` (`observation_external_account_id`),
  KEY `observation_external_english_account_id` (`observation_external_english_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_record_detail`
--

DROP TABLE IF EXISTS `school_record_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_record_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `school_record_id` int(10) unsigned NOT NULL DEFAULT '0',
  `school_record_section_id` int(10) unsigned NOT NULL DEFAULT '0',
  `grade` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `school_record_id` (`school_record_id`),
  KEY `school_record_section_id` (`school_record_section_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_record_section`
--

DROP TABLE IF EXISTS `school_record_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_record_section` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_term`
--

DROP TABLE IF EXISTS `school_term`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_term` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `school_year_id` int(10) unsigned NOT NULL DEFAULT '0',
  `school_term_type_id` int(10) unsigned NOT NULL DEFAULT '0',
  `start_date` date NOT NULL DEFAULT '0000-00-00',
  `end_date` date NOT NULL DEFAULT '0000-00-00',
  `name` varchar(128) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `school_year_id` (`school_year_id`),
  KEY `school_term_type_id` (`school_term_type_id`),
  KEY `start_date` (`start_date`),
  KEY `end_date` (`end_date`)
) ENGINE=InnoDB AUTO_INCREMENT=165 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_term_type`
--

DROP TABLE IF EXISTS `school_term_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_term_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `month` int(2) unsigned NOT NULL DEFAULT '0',
  `sequence` int(2) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `school_year`
--

DROP TABLE IF EXISTS `school_year`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_year` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `year` date NOT NULL DEFAULT '0000-00-00',
  `name` varchar(128) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `shift_attendance`
--

DROP TABLE IF EXISTS `shift_attendance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shift_attendance` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_shift_work_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_authorized_absent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_leave_absent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_temporary_return` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `comment` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `class_shift_work_id` (`class_shift_work_id`)
) ENGINE=InnoDB AUTO_INCREMENT=54596 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `staff`
--

DROP TABLE IF EXISTS `staff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `staff_no` varchar(128) NOT NULL DEFAULT '',
  `employment_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `entry_date` date NOT NULL DEFAULT '0000-00-00',
  `retirement_date` date NOT NULL DEFAULT '0000-00-00',
  `retirement_reason` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `staff_no` (`staff_no`)
) ENGINE=InnoDB AUTO_INCREMENT=1906 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `staff_ability`
--

DROP TABLE IF EXISTS `staff_ability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff_ability` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ability_group_id` int(11) NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `staff_id` (`staff_id`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `staff_qualification`
--

DROP TABLE IF EXISTS `staff_qualification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staff_qualification` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `staff_id` int(10) unsigned NOT NULL DEFAULT '0',
  `external_test_level_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `staff_id` (`staff_id`),
  KEY `external_test_level_id` (`external_test_level_id`),
  KEY `lastup_account_id` (`lastup_account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `student` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `student_no` varchar(128) NOT NULL DEFAULT '',
  `course_id` int(10) unsigned NOT NULL DEFAULT '0',
  `course_length_month` int(10) unsigned NOT NULL DEFAULT '0',
  `ability_id` int(10) unsigned NOT NULL DEFAULT '0',
  `start_date` date NOT NULL DEFAULT '0000-00-00',
  `end_date_intended` date NOT NULL DEFAULT '0000-00-00',
  `end_date` date NOT NULL DEFAULT '0000-00-00',
  `weekly_study_hours` int(10) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `quit_reason_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `student_type_cd` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `after_graduate_school` varchar(255) NOT NULL DEFAULT '',
  `part_time_job_name` varchar(255) NOT NULL DEFAULT '',
  `part_time_job_address1` varchar(255) NOT NULL DEFAULT '',
  `part_time_job_address2` varchar(255) NOT NULL DEFAULT '',
  `part_time_job_address3` varchar(255) NOT NULL DEFAULT '',
  `part_time_job_phone_no` varchar(20) NOT NULL DEFAULT '',
  `continuous_absent_status` text NOT NULL,
  `old_class_name` varchar(128) NOT NULL DEFAULT '',
  `is_old_class` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `course_id` (`course_id`),
  KEY `ability_id` (`ability_id`),
  KEY `student_no` (`student_no`)
) ENGINE=InnoDB AUTO_INCREMENT=1816 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `system_code`
--

DROP TABLE IF EXISTS `system_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_code` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `column_name` varchar(128) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `column_name` (`column_name`)
) ENGINE=InnoDB AUTO_INCREMENT=468 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `system_code_detail`
--

DROP TABLE IF EXISTS `system_code_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `system_code_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `system_code_id` int(10) unsigned NOT NULL DEFAULT '0',
  `code1` int(3) unsigned NOT NULL DEFAULT '0',
  `code2` int(3) unsigned NOT NULL DEFAULT '0',
  `code3` int(3) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `column_name` varchar(128) NOT NULL DEFAULT '',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `system_code_id` (`system_code_id`)
) ENGINE=InnoDB AUTO_INCREMENT=488 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tablet_application_update`
--

DROP TABLE IF EXISTS `tablet_application_update`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tablet_application_update` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `version_name` varchar(128) NOT NULL DEFAULT '',
  `download_url` varchar(255) NOT NULL DEFAULT '',
  `is_for_staff` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_for_student` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_latest_for_staff` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_latest_for_student` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `teacher_class_history`
--

DROP TABLE IF EXISTS `teacher_class_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher_class_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `class_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_absent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_teacher_in_charge` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `total_class_time` int(10) unsigned NOT NULL DEFAULT '0',
  `period_wage` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `school_term_id` (`school_term_id`),
  KEY `class_id` (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `teacher_school_term`
--

DROP TABLE IF EXISTS `teacher_school_term`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teacher_school_term` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `school_term_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date_from` date NOT NULL DEFAULT '0000-00-00',
  `date_to` date NOT NULL DEFAULT '0000-00-00',
  `period_wage` int(10) unsigned NOT NULL DEFAULT '0',
  `work_days_week` int(10) unsigned NOT NULL DEFAULT '0',
  `work_times_week` int(10) unsigned NOT NULL DEFAULT '0',
  `is_full_day_request` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_teacher_in_charge` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `comment_request` text NOT NULL,
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `person_id` (`person_id`),
  KEY `school_term_id` (`school_term_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `temporary_attendance_time`
--

DROP TABLE IF EXISTS `temporary_attendance_time`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temporary_attendance_time` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `person_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `class_shift_work_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `person_id` (`person_id`),
  KEY `shift_attendance_id` (`class_shift_work_id`),
  CONSTRAINT `temporary_attendance_time_ibfk_1` FOREIGN KEY (`class_shift_work_id`) REFERENCES `class_shift_work` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `column_name` varchar(128) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `column_name` (`column_name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `test_section`
--

DROP TABLE IF EXISTS `test_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_section` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `test_level_id` int(10) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `total_point` int(10) unsigned NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `test_level_id` (`test_level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time_table`
--

DROP TABLE IF EXISTS `time_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time_table` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `time_table_group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `time_from` time NOT NULL DEFAULT '00:00:00',
  `time_to` time NOT NULL DEFAULT '00:00:00',
  `period_id` int(10) unsigned NOT NULL DEFAULT '0',
  `is_break` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `time_table_group_id` (`time_table_group_id`),
  KEY `period_id` (`period_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time_table_group`
--

DROP TABLE IF EXISTS `time_table_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time_table_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `time_zone`
--

DROP TABLE IF EXISTS `time_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `time_zone` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `sequence` int(10) unsigned NOT NULL DEFAULT '0',
  `lastup_account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `create_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastup_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-10 10:28:21
