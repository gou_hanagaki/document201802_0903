CREATE TABLE IF NOT EXISTS `temporary_attendance_time` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`account_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`person_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`entry_date_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`class_shift_work_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`lastup_account_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`create_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`lastup_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`disable` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	INDEX account_id (`account_id`), 
	INDEX person_id (`person_id`),
	INDEX class_shift_work_id (`class_shift_work_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;