-- Correct system_code attendance_rate_cd
-- This base #1026 
-- This SQL also existed on #1026 SQL
UPDATE system_code_detail 
JOIN system_code ON system_code_detail.`system_code_id` = system_code.`id`
SET code1 = 3
WHERE system_code.`column_name` = 'attendance_rate_cd'
AND system_code_detail.`column_name` = 'monthly';

UPDATE system_code_detail 
JOIN system_code ON system_code_detail.`system_code_id` = system_code.`id`
SET code1 = 1
WHERE system_code.`column_name` = 'attendance_rate_cd'
AND system_code_detail.`column_name` = 'current';

-- Reset and correct data for attendance_rate_rank
TRUNCATE TABLE `attendance_rate_rank`;

INSERT INTO `attendance_rate_rank` (`id`, `color`, `threshold_rate`, `lastup_account_id`, `create_datetime`, `lastup_datetime`, `disable`) VALUES
    (1, '00ff00', 1000, 0, NOW(), NOW(), 0),
    (2, '0080ff', 900, 0, NOW(), NOW(), 0),
    (3, 'ff8000', 800, 0, NOW(), NOW(), 0),
    (4, 'ff0000', 700, 0, NOW(), NOW(), 0),
    (5, 'ffffff', 10, 0, NOW(), NOW(), 0);

INSERT INTO `function` (`function_group_id`, `name`, `path`, `is_in_menu`, `sequence`, `lastup_account_id`, `create_datetime`) VALUES
(6,	'出席率ランキング',	'staff/export/ranking_attendance_rate',	1,	1,	2000,	now());
