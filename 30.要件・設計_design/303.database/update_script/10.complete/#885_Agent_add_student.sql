ALTER TABLE student
ADD COLUMN agent_id int(10) UNSIGNED NOT NULL DEFAULT 0 AFTER person_id;
ALTER TABLE `immigration` DROP COLUMN `agent_id`;