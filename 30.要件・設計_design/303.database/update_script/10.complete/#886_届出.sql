-- #886
-- -----------------------------------------------------
-- Table `application`
-- -----------------------------------------------------
ALTER TABLE `application`
ADD COLUMN `confirmed_absent_date_to` date NOT NULL DEFAULT '0000-00-00' AFTER `authorized_absent_date_to`;

ALTER TABLE `application`
ADD COLUMN `confirmed_absent_date_from` date NOT NULL DEFAULT '0000-00-00' AFTER `authorized_absent_date_to`;