﻿ALTER TABLE `person`
ADD COLUMN `abbreviation_name` varchar(128) NOT NULL DEFAULT '' AFTER `legal_registered_first_name_alphabet`;

ALTER TABLE `shift_attendance`
ADD COLUMN `is_temporary_return` tinyint(1) unsigned NOT NULL DEFAULT '0' AFTER is_leave_absent;

ALTER TABLE `attendance`
ADD COLUMN `is_temporary_return` tinyint(1) unsigned NOT NULL DEFAULT '0' AFTER is_canceled;